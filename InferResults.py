import time
import numpy as np
from skimage.measure import compare_ssim

try:
    from collections import OrderedDict
except ImportError:
    OrderedDict = dict


class InferResult:
    def __init__(self,number_of_iterations):
        self.r_RMSE=0
        self.r_abs_diff=0
        self.r_abs_rel=0
        self.r_inv_rel=0
        self.r_sc_inv=0
        self.r_a1=0
        self.r_a2=0
        self.r_a3=0
        self.r_time=0
        self.end_time=time.time()
        self.counter=0
        self.pbar=None
        self.table_of_results=np.zeros((number_of_iterations,10))
    def setPBar(self,pbar):
        self.pbar=pbar
    def update_metrics(self,loss_metrics,loss_RMSE,background_depth,output_depth):
        # abs_diff, abs_rel, inv_rel, sc_inv, a1, a2, a3=loss_metrics
        self.r_RMSE += loss_RMSE
        self.r_abs_diff+=loss_metrics[0]
        self.r_abs_rel+=loss_metrics[1]
        self.r_inv_rel+=loss_metrics[2]
        self.r_sc_inv+=loss_metrics[3]
        self.r_a1+=loss_metrics[4]
        self.r_a2+=loss_metrics[5]
        self.r_a3+=loss_metrics[6]
        partial_time=time.time()-self.end_time
        self.r_time+=partial_time
        self.end_time=time.time()
        self.pbar.set_postfix(OrderedDict(
            running_time="{:.4f}".format( self.r_time/ (self.counter + 1)),
            RMSE="{:.4f}".format(self.r_RMSE /(self.counter + 1)),
            abs_diff="{:.4f}".format(self.r_abs_diff / (self.counter + 1)),
            sc_inv="{:.4f}".format(self.r_sc_inv / (self.counter + 1)),
            abs_rel="{:.4f}".format(self.r_abs_rel / (self.counter + 1))
        ))
        self.table_of_results[self.counter,0]=loss_RMSE
        self.table_of_results[self.counter,1]=loss_metrics[0]
        self.table_of_results[self.counter,2]=loss_metrics[1]
        self.table_of_results[self.counter,3]=loss_metrics[2]
        self.table_of_results[self.counter,4]=loss_metrics[3]
        self.table_of_results[self.counter,5]=loss_metrics[4]
        self.table_of_results[self.counter,6]=loss_metrics[5]
        self.table_of_results[self.counter,7]=loss_metrics[6]
        self.table_of_results[self.counter,8]=partial_time
        (score,diff)=compare_ssim(output_depth, background_depth, full=True)
        self.table_of_results[self.counter,9]=score
        self.counter+=1
    def saveToCSV(self,path_to_file):
        np.savetxt(path_to_file,self.table_of_results,delimiter=";")
    def __repr__(self):
        return "InferResults()"
