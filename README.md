This is a package for master thesis(CNN for monocular depth estimation with correlation layers in picking tasks) done by Ryota Yoneyama at universitad Jaume I.

-The requirements of the codes are written in requirements.txt.

- The directory composed of datasets, models, trained_models and executable python codes.
  - models: networks (DepthC, DepthS_solo, DepthS for DepthCSx )
  - trained_models: checkpoints of learned weights
  - datasets: dataloader
  - executable codes( trainCorS.py, trainCS.py, trainCSS.py, inferCSx.py)

- The examples how to run codes are as follows

  - python3 inferCSx.py --mode c (or s or cs or css)
    which infers depth maps with learned weights from trained_models directory.
    Please make sure Path for checkpoints, datasets and save directory are correct.

  - python3 trainCSS.py or python3 trainCSS.py --weight learned (with learned weights)
    which trains DepthCSS.
    Please make sure Path for checkpoints, datasets and save directory are correct.

  - python3 trainCS.py or python3 trainCS.py --weight learned (with learned weights)
    which trains DepthCS.
    Please make sure Path for checkpoints, datasets and save directory are correct.

  - python3 trainCorS.py --mode c (or s) or --weight learned (with learned weights)
    which trains DepthC or DepthS.
    Please make sure Path for checkpoints, datasets and save directory are correct.


if you have a question, please contact me ( al388242@uji.es ).

-Using Anaconda environment to execute:

* Anaconda3 installation:
1. cd $HOME
2. mkdir tmp && cd tmp
3. curl -O https://repo.anaconda.com/archive/Anaconda3-2019.10-Linux-x86_64.sh
4. bash Anaconda3-2019.10-Linux-x86_64.sh

5. if no conda init option has been choosed:
      source ~/.bashrc

* Create conda environment for python 3
1. conda create -n python3_env python=3
2. conda activate python3_env
3. cd $HOME && git clone https://gitlab.com/depth_by_deep/depth_estimation.git
4. cd depth_estimation

* Install required packages

1. conda install tensorflow tensorboard
2. conda install pytorch torchvision cudatoolkit=10.1 -c pytorch
3. conda install opencv
4. pip install pyquaternion
5. conda install matplotlib
6. conda install imageio
7. conda install scikit-image
8. conda install tqdm
9. pip install spatial-correlation-sampler
10. conda install pandas
