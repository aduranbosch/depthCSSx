#!/bin/bash

args=( "$@" )
total=${#args[*]}
root_dir=$(pwd)
mode=${args[-3]}
epcs=${args[-2]}
output=${args[-1]}

for (( i=0; i<=$(( $total -4 )); i++ ))
do
    var=${args[$i]}
    data_path=$data_path" /home/"$USER"/trained_models/depth_"$mode"_logger_to_"${var}"_epc_"$epcs"/"
    echo "Saving data to /home/"$USER"/trained_models/depth_"$mode"_logger_to_"${var}"_epc_"$epcs"/"$output
done
python3 extracting_learning_curve.py --dir_list $data_path -o $output
