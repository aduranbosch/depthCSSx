import torch.utils.data as data
import os
import os.path
from imageio import imread
import numpy as np
import torch
from skimage import io, transform
import sys
import cv2
NORMALIZE_LENGTH = 0.00334632
DEPTH_MAX = 5.0
# NORMALIZE_LENGTH = 0.35

class ClassifyDataset(data.Dataset):
    """Face Landmarks dataset."""

    def __init__(self, root_dir, transform, path_list):
    # def __init__(self, args, root_dir, transform=None,is_cropped = False):

        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """

        # self.is_cropped = is_cropped
        # self.crop_size = args.crop_size
        # self.render_size = args.inference_size
        self.root_dir = root_dir
        self.transform = transform
        self.path_list = path_list


    def __len__(self):
        return len(self.path_list)
        # return 10

    def __getitem__(self, iter):
        imgs, deltas, depth = self.path_list[iter]
        vx =  np.loadtxt(deltas[0], delimiter=',', skiprows=0)/NORMALIZE_LENGTH
        vy =  np.loadtxt(deltas[1], delimiter=',', skiprows=0)/NORMALIZE_LENGTH
        vz =  np.loadtxt(deltas[2], delimiter=',', skiprows=0)/NORMALIZE_LENGTH
        # vx = np.round(vx*127.5+127.5)
        # vy = np.round(vy*127.5+127.5)
        # vz = np.round(vz*127.5+127.5)
        depth =  np.loadtxt(depth,delimiter=',', skiprows=0)

        max = np.max(depth)
        if max < DEPTH_MAX:
            depth = depth = np.round(depth*255.0/DEPTH_MAX)
        else:
            depth = depth = np.round(depth*255.0/max)

        depth = cv2.resize(depth, dsize=(64, 64), interpolation=cv2.INTER_LINEAR)
        depth = torch.from_numpy(depth).long()

        img1 = io.imread(imgs[0])
        img2 = io.imread(imgs[1])
        vels = np.dstack([vx,vy,vz])
        inputs = [img1, img2,vels]
        inputs[0] = self.transform(inputs[0])
        inputs[1] = self.transform(inputs[1])
        inputs[2] = np.transpose(inputs[2], (2, 0, 1))
        inputs[2] = torch.from_numpy(inputs[2]).float()
        return inputs, depth
