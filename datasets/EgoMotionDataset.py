import torch.utils.data as data
import os
import os.path
from imageio import imread
import numpy as np
import torch
from skimage import io, transform
import sys
import cv2
import PIL.Image
from pyquaternion import Quaternion

FOCUS_LENGTH = 286.02185  # [pixel] fx=fy from gazebo definition
NEAR_PLANE = 0.0012 #[m]
CX = 240.5 # [pixel] width = 480
CY = 150.5 # [pixel] height = 300
PIXEL_SIZE = NEAR_PLANE / FOCUS_LENGTH # [m/pixel] sx = sy
FOCUS_LENGTH_INFER = 405.76240046  # [pixel] fx=fy from gazebo definition
NEAR_PLANE_INFER = 0.0012 #[m]
CX_INFER = 480.5 # [pixel] width = 960
CY_INFER = 300.5 # [pixel] height = 600
PIXEL_SIZE_INFER = NEAR_PLANE_INFER / FOCUS_LENGTH_INFER # [m/pixel] sx = sy
# NORMALIZE_LENGTH = 0.053984
NORMALIZE_LENGTH = 0.03
DEPTH_MAX = 2.0
DEPTH_MIN = 0.0012

# Function to add gaussian noises
def gaussian(ins, mean, stddev):
    noise = torch.randn(ins.shape)*stddev +mean
    return ins + noise




class EgoMotionTxtDataset(data.Dataset):

    def __init__(self, root_dir, transform, path_list):

        self.root_dir = root_dir
        self.transform = transform
        self.path_list = path_list
        dirs= [ name for name in os.listdir(root_dir) if os.path.isdir(os.path.join(root_dir, name)) ]
        #dirs=os.listdir(root_dir)
        image = root_dir + "/"+dirs[0]+"/my_image0.png"

        im = PIL.Image.open(image)
        self.w, self.h = im.size

    def __len__(self):
        return len(self.path_list)

    def __getitem__(self, iter):
        # imgs-> a pair of images
        # Mat: transform vectors between source and target images =(tx,ty,tz, qx,qy,qz,qw)
        # depth:= ground truth
        imgs, Mats, depth = self.path_list[iter]

        Mat1 = np.loadtxt(Mats[0],delimiter=',', skiprows=0)
        my_quaternion1 = Quaternion(Mat1[6],Mat1[3],Mat1[4],Mat1[5])
        c1Tb = my_quaternion1.transformation_matrix
        c1Tb[0:3,3] = Mat1[:3] + np.random.normal(0,0.001,3)
        #c1Tb : transform from base to source camera frame

        Mat2 = np.loadtxt(Mats[1],delimiter=',', skiprows=0)
        my_quaternion2 =  Quaternion(Mat2[6],Mat2[3],Mat2[4],Mat2[5])
        c2Tb = my_quaternion2.transformation_matrix
        c2Tb[0:3,3] = Mat2[:3] + np.random.normal(0,0.001,3)
        #c2Tb : transform from base to target camera frame

        c2Tc1 = np.dot(c2Tb,np.linalg.pinv(c1Tb))
        c1Tc2 = np.linalg.pinv(c2Tc1)
        pose = c1Tc2[0:3,:]
        pose = torch.from_numpy(pose).float()

        vx = np.zeros([self.h,self.w])
        vy = np.zeros([self.h,self.w])
        vz = np.zeros([self.h,self.w])

        # To calculate displacements on each pixels of image planes
        # Parameters of the image plane is defined at top of this code.
        for row in range(self.h):
                x = np.linspace(1-CX,self.w-CX,self.w)*PIXEL_SIZE
                y = np.ones(self.w)*(row+1)
                z = np.ones(self.w)*NEAR_PLANE
                I = np.ones(self.w)
                P = np.array([x,y,z,I])
                #(dPx,dPy, dPz) =  T(c1->c2)*P - P
                dP = np.linalg.pinv(c2Tc1).dot(P)-P
                vx[row,:] = dP[0,:]
                vy[row,:] = dP[1,:]
                vz[row,:] = dP[2,:]

        # To load ground truth of depth and image inputs and resize them.
        depth =  np.loadtxt(depth,delimiter=',', skiprows=0) # Normalization by near plane
        depth = cv2.resize(depth, dsize=(240, 150), interpolation=cv2.INTER_LINEAR)
        depth = torch.from_numpy(depth).float()

        img1 = io.imread(imgs[0])
        img2 = io.imread(imgs[1])
        img1 = cv2.resize(img1, dsize=(480, 300), interpolation=cv2.INTER_LINEAR)
        img2 = cv2.resize(img2, dsize=(480, 300), interpolation=cv2.INTER_LINEAR)

        images= [img1, img2]
        # To transform images from RGB to tensor with normalization
        images[0] = self.transform(images[0])
        images[1] = self.transform(images[1])
        # To add gaussian noise
        images[0] = gaussian(images[0], 0, 0.07)
        images[1] = gaussian(images[1], 0, 0.07)

        vels = np.dstack([vx,vy,vz])
        vels = cv2.resize(vels, dsize=(480, 300), interpolation=cv2.INTER_LINEAR)
        vels = np.transpose(vels, (2, 0, 1))
        vels = torch.from_numpy(vels).float()

        return images, pose, depth, vels


class RealDataset(data.Dataset):

    def __init__(self, root_dir, transform, path_list):
    # def __init__(self, args, root_dir, transform=None,is_cropped = False):

        """
        Args:
        """

        # self.is_cropped = is_cropped
        # self.crop_size = args.crop_size
        # self.render_size = args.inference_size
        self.root_dir = root_dir
        self.transform = transform
        self.path_list = path_list
        self.w = 480
        self.h = 300

    def __len__(self):
        return len(self.path_list)
        # return 10

    def __getitem__(self, iter):
        imgs, Mats = self.path_list[iter]

        Mat1 = np.loadtxt(Mats[0],delimiter=',', skiprows=0)
        my_quaternion1 = Quaternion(Mat1[6],Mat1[3],Mat1[4],Mat1[5])
        bTc1 = my_quaternion1.transformation_matrix
        bTc1[0:3,3] = Mat1[:3]

        Mat2 = np.loadtxt(Mats[1],delimiter=',', skiprows=0)
        my_quaternion2 =  Quaternion(Mat2[6],Mat2[3],Mat2[4],Mat2[5])
        bTc2 = my_quaternion2.transformation_matrix
        bTc2[0:3,3] = Mat2[:3]

        c1Tc2 = np.dot(np.linalg.pinv(bTc1),bTc2)


        vx = np.zeros([self.h,self.w])
        vy = np.zeros([self.h,self.w])
        vz = np.zeros([self.h,self.w])


        for row in range(self.h):
                x = np.linspace(1-CX_INFER,self.w-CX_INFER,self.w)*PIXEL_SIZE_INFER
                y = np.ones(self.w)*(row+1)
                z = np.ones(self.w)*NEAR_PLANE_INFER
                I = np.ones(self.w)
                P = np.array([x,y,z,I])
                dP = c1Tc2.dot(P)-P
                vx[row,:] = dP[0,:]/NORMALIZE_LENGTH
                vy[row,:] = dP[1,:]/NORMALIZE_LENGTH
                vz[row,:] = dP[2,:]/NORMALIZE_LENGTH




        img1 = io.imread(imgs[0])
        img2 = io.imread(imgs[1])
        vels = np.dstack([vx,vy,vz])

        img1 = cv2.resize(img1, dsize=(480, 300), interpolation=cv2.INTER_LINEAR)
        img2 = cv2.resize(img2, dsize=(480, 300), interpolation=cv2.INTER_LINEAR)
        vels = cv2.resize(vels, dsize=(480, 300), interpolation=cv2.INTER_LINEAR)

        inputs = [img1, img2,vels]
        inputs[0] = self.transform(inputs[0])
        inputs[1] = self.transform(inputs[1])
        inputs[2] = np.transpose(inputs[2], (2, 0, 1))
        inputs[2] = torch.from_numpy(inputs[2]).float()
        return inputs
