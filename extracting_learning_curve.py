import argparse
import numpy as np
import sys
from tensorboard.backend.event_processing.event_accumulator import EventAccumulator

def extracting_learning_curve(dir_list,output_filename):
    for dir in dir_list:
        # Train logger processing
        print ("processing "+dir+"train/")
        train=EventAccumulator(path=dir+"train/")
        test=EventAccumulator(path=dir+"test/")
        train.Reload()
        test.Reload()
        train.FirstEventTimestamp()
        test.FirstEventTimestamp()
        train_data=dict()
        for sca in train.Scalars('train_loss_ave'):
            train_data[sca.step]=sca.value
        results=[]
        for sca in test.Scalars('test_loss_ave'):
            if sca.step in train_data:
                results.append([sca.step,train_data[sca.step],sca.value])
        file_data=np.array(results)
        path_to_save=dir+output_filename
        print ('Saving data to '+path_to_save)
        np.savetxt(path_to_save, file_data, delimiter=";")
    return 0
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-d', '--dir_list',  # either of this switches
        nargs='+',       # one or more parameters to this switch
        type=str,        # /parameters/ are ints
        dest='dir_list',     # store in 'list'.
        default=[],      # since we're not specifying required.
    )
    parser.add_argument('-o','--output_filename')
    args = parser.parse_args()
    res=extracting_learning_curve(args.dir_list, args.output_filename)
    sys.exit(res)
