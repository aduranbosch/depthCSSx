#!/usr/bin/python
import numpy as np
import os
from distutils.dir_util import copy_tree
import shutil

def split_dataset(source,n_samples_in_source,target,n_samples_target,original_path):
    selected_folders=[]
    if not os.path.exists(target):
        os.mkdir(target)
        print("Directory " , target ,  " Created ")
    else:
        print("Directory " , target ,  " already exists")
        print("!!! THE EXISTING DATASET WILL BE EREASED !!!!")
        raw_input("Press Enter to continue...")
        shutil.rmtree(target)
        os.mkdir(target)
    no_used=np.loadtxt(source+"/no_used_indexes.csv");

    for ind in no_used[0:n_samples_target+1]:
        source_path=original_path+"/scene_"+str(np.int16(np.round(ind)))
        target_path=target+"/scene_"+str(np.int16(np.round(ind)))
        print("cp %s -->  %s")%(source_path,target_path)
        copy_tree(source_path,target_path)

if __name__ == "__main__":

    n_samples_in_target=600
    #n_samples_in_source=[500,700,1000,2000,3000,4000,5000,6000,7000,8000,9000];
    n_samples_in_source=[6000,7000,8000,9000];
    for i in range(len(n_samples_in_source)):
        target_path="/home/ryota/baxter_datasets/infer_"+str(n_samples_in_source[i])
        source_path="/home/ryota/baxter_datasets/train_data_"+str(n_samples_in_source[i])
        original_path="/media/ryota/TOSHIBA EXT/RYOTA_DATASETS/train_data"
        print("NUMBER OF TARGET SAMPLES:................%d")%(n_samples_in_source[i])
        split_dataset(source_path,n_samples_in_source,target_path,n_samples_in_target,original_path)
