#!/usr/bin/python3

from __future__ import print_function, division
import torch.optim as optim
import os
import torch
import pandas as pd
from skimage import io, transform
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import utils
import glob
import warnings
import torch.nn as nn
import argparse
import shutil
import time

import torch
import torch.nn.functional as F
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data as data
import torchvision.transforms as transforms
import models
import datetime
from tensorboardX import SummaryWriter
import numpy as np
from torch.nn.init import kaiming_normal_, constant_
from models.util import conv, predict_flow, deconv, crop_like, correlate, correlate_patch
import sys
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2
import torchvision.utils as vutils
from models import DepthCorrNet
from datasets import EgoMotionDataset
from ryota_utils import split2list, makelist,  makelist_infer, gen_plt, makelist_infer_duran, motion_vec2mat
from inverse_warp import inverse_warp
from loss_functions import photometric_reconstruction_loss, explainability_loss, smooth_loss, compute_errors, RMSELoss, EigenLoss


from skimage.measure import compare_ssim
# import skorch
# from skorch import NeuralNetClassifier

import matplotlib as mpl
import matplotlib.image as mpimg
from PIL import Image

from tqdm import tqdm

try:
    from collections import OrderedDict
except ImportError:
    OrderedDict = dict

from InferResults import InferResult


def inferCSS(inferloader, infer_writer, modelC, modelS1, modelS2, intrinsics_mat, results_path,loop=1):
    modelC.eval()
    modelS1.eval()
    modelS2.eval()
    running_RMSE, running_abs_diff, running_abs_rel, running_inv_rel, running_sc_inv, running_a1, running_a2, running_a3, running_time = 0, 0, 0, 0, 0, 0, 0, 0, 0

    running_time = 0.0
    #print(len(inferloader))
    results=InferResult(len(inferloader))
    with tqdm(inferloader, ncols=150) as pbar:
        results.setPBar(pbar)
        for i, data in enumerate(pbar):
            end = time.time()
            images, motion_mat, depth, vels = data
            depth = depth.to(device)
            images[0] = images[0].to(device)
            images[1] = images[1].to(device)
            motion_mat = motion_mat.to(device)
            vels = vels.to(device)

            depth1 = modelC([images[0], images[1], vels])
            if depth1.dim() == 4:
                depth1 = torch.squeeze(depth1, dim=1)

            loss_photo1, warped_1, diff_1 = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                                            depth1, None, motion_mat, 'euler', 'zeros')

            warped_hidden, diff_hidden, depth_hidden = warped_1, diff_1, depth1
            for looper in range(loop):
                depth_hidden, motion_vec_hidden = modelS1(images, warped_hidden, diff_hidden, depth_hidden)
                if depth_hidden.dim() == 4:
                    depth_hidden = torch.squeeze(depth_hidden)

                motion_mat_hidden = motion_vec2mat(motion_vec_hidden)
                loss_photo_hidden, warped_hidden, diff_hidden = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                                                                depth_hidden, None, motion_mat_hidden,
                                                                                                'euler', 'zeros')
            depthF, motion_vecF = modelS2(images, warped_hidden, diff_hidden, depth_hidden)
            if depthF.dim() == 4:
                depthF = torch.squeeze(depthF)
            motion_matF = motion_vec2mat(motion_vecF)
            loss_photoF, warped_F, diff_F = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                                            depthF, None, motion_matF,
                                                                            'euler', 'zeros')

            error_map = torch.abs(depthF-depth)
            loss_RMSE = criterion(depthF, depth)
            output_depth=depthF.cpu()[0].numpy()
            background_depth=depth.cpu()[0].numpy()
            loss_metrics = compute_errors(depth, depthF)
            # abs_diff, abs_rel, inv_rel, sc_inv, a1, a2, a3 = loss_metrics

            running_RMSE += loss_RMSE.item()
            results.update_metrics(loss_metrics,loss_RMSE.item(),background_depth,output_depth)
            # running_RMSE += loss_RMSE.item()
            # running_abs_diff += abs_diff
            # running_abs_rel += abs_rel
            # running_inv_rel += inv_rel
            # running_sc_inv += sc_inv
            # running_a1 += a1
            # running_a2 += a2
            # running_a3 += a3
            # running_time += time.time() - end
            # end = time.time()
            # pbar.set_postfix(OrderedDict(
            #     running_time="{:.4f}".format(running_time / (i + 1)),
            #     RMSE="{:.4f}".format(running_RMSE / (i + 1)),
            #     abs_diff="{:.4f}".format(running_abs_diff / (i + 1)),
            #     sc_inv="{:.4f}".format(running_sc_inv / (i + 1)),
            #     abs_rel="{:.4f}".format(running_abs_rel / (i + 1))
            # ))
            i += 1

            image_nows = images[1]
            image_pasts = images[0]
            image_now = vutils.make_grid(
            image_nows[0], normalize=True, scale_each=True)
            image_now = vutils.make_grid(
                image_nows[0], normalize=True, scale_each=True)
            image_past = vutils.make_grid(
                image_pasts[0], normalize=True, scale_each=True)
            label_image = Image.open(gen_plt(depth.cpu()[0],sc_c=[0.0,1.2]))
                # label_image.save("/home/ryota/Desktop/test/" +"label" + str(i) + ".png", quality=95)
            label_image = transforms.ToTensor()(label_image)
            error_image = Image.open(gen_plt(error_map.cpu()[0],sc_c=[0,0.3]))
            error_image = transforms.ToTensor()(error_image)
            output_image = Image.open(gen_plt(depthF.data.cpu()[0],sc_c=[0.0,1.2]))
            output_image = transforms.ToTensor()(output_image)
                # depthCS_image = Image.open(gen_plt(depth_hidden.data.cpu()[0]))
                # depthCS_image.save("/home/ryota/Desktop/testCS/" +"depthCS" + str(i) + ".png", quality=95)
                # depthC_image = Image.open(gen_plt(depth1.data.cpu()[0]))
                # depthC_image.save("/home/ryota/Desktop/testC/" +"depthC" + str(i) + ".png", quality=95)
            infer_writer.add_image('infer_Image_n', image_now, i + 1)
            infer_writer.add_image('infer_Image_n-1', image_past, i + 1)
            infer_writer.add_image('infer_Depth_GroundTruth', label_image, i + 1)
            infer_writer.add_image('infer_Depth_Output', output_image, i + 1)
            infer_writer.add_image('infer_error_Output', error_image, i + 1)
            infer_writer.add_scalar('running_RMSE', running_RMSE, i + 1)

    # print("Average RMSE:", running_RMSE / (i + 1))
    # print("Average abs_diff:", running_abs_diff / (i + 1))
    # print("Average abs_rel:", running_abs_rel / (i + 1))
    # print("Average inv_rel:", running_inv_rel / (i + 1))
    # print("Average sc_inv:", running_sc_inv / (i + 1))
    # print("Average a1:", running_a1 / (i + 1))
    # print("Average a2:", running_a2 / (i + 1))
    # print("Average a3:", running_a3 / (i + 1))
    # print("Number of data:", i)
    # print("Average time:", running_time / i)
    np.set_printoptions(linewidth=np.inf)
    print(np.hstack((np.mean(results.table_of_results,axis=0),np.std(results.table_of_results,axis=0))))
    results.saveToCSV(results_path)

def inferCS(inferloader, infer_writer, modelC, modelS1, intrinsics_mat,results_path):
    modelC.eval()
    modelS1.eval()
    running_RMSE, running_abs_diff, running_abs_rel, running_inv_rel, running_sc_inv, running_a1, running_a2, running_a3, running_time = 0, 0, 0, 0, 0, 0, 0, 0, 0
    running_time = 0.0

    #print(len(inferloader))
    results=InferResult(len(inferloader))
    with tqdm(inferloader, ncols=150) as pbar:
        results.setPBar(pbar)
        for i, data in enumerate(pbar):
            end = time.time()
            images, motion_mat, depth, vels = data
            depth = depth.to(device)
            images[0] = images[0].to(device)
            images[1] = images[1].to(device)
            motion_mat = motion_mat.to(device)
            vels = vels.to(device)

            depth1 = modelC([images[0], images[1], vels])
            if depth1.dim() == 4:
                depth1 = torch.squeeze(depth1, dim=1)

            loss_photo1, warped_1, diff_1 = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                                            depth1, None, motion_mat, 'euler', 'zeros')

            warped_hidden, diff_hidden, depth_hidden = warped_1, diff_1, depth1
            loop=1
            for looper in range(loop):
                depth_hidden, motion_vec_hidden = modelS1(images, warped_hidden, diff_hidden, depth_hidden)
                if depth_hidden.dim() == 4:
                    depth_hidden = torch.squeeze(depth_hidden)

                motion_mat_hidden = motion_vec2mat(motion_vec_hidden)
                loss_photo_hidden, warped_hidden, diff_hidden = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                                                                depth_hidden, None, motion_mat_hidden,
                                                                                                'euler', 'zeros')
            depthF = depth_hidden


            error_map = torch.abs(depthF-depth)
            loss_RMSE = criterion(depthF, depth)
            output_depth=depthF.cpu()[0].numpy()
            background_depth=depth.cpu()[0].numpy()
            loss_metrics = compute_errors(depth, depthF)
            #abs_diff, abs_rel, inv_rel, sc_inv, a1, a2, a3 = loss_metrics


            running_RMSE += loss_RMSE.item()
            results.update_metrics(loss_metrics,loss_RMSE.item(),background_depth,output_depth)
            # running_abs_diff += abs_diff
            # running_abs_rel += abs_rel
            # running_inv_rel += inv_rel
            # running_sc_inv += sc_inv
            # running_a1 += a1
            # running_a2 += a2
            # running_a3 += a3
            # running_time += time.time() - end
            # end = time.time()
            # pbar.set_postfix(OrderedDict(
            #     running_time="{:.4f}".format(running_time / (i + 1)),
            #     RMSE="{:.4f}".format(running_RMSE / (i + 1)),
            #     abs_diff="{:.4f}".format(running_abs_diff / (i + 1)),
            #     sc_inv="{:.4f}".format(running_sc_inv / (i + 1)),
            #     abs_rel="{:.4f}".format(running_abs_rel / (i + 1))
            # ))
            i += 1

            #if i % 1 == 1 - 1:
            image_nows = images[1]
            image_pasts = images[0]
            image_now = vutils.make_grid(
                image_nows[0], normalize=True, scale_each=True)
            image_past = vutils.make_grid(
                image_pasts[0], normalize=True, scale_each=True)

            label_image = Image.open(gen_plt(depth.cpu()[0],sc_c=[0.0,1.2]))
                # label_image.save("/home/ryota/Desktop/test/" +"label" + str(i) + ".png", quality=95)
            label_image = transforms.ToTensor()(label_image)
            error_image = Image.open(gen_plt(error_map.cpu()[0],sc_c=[0,0.3]))
            error_image = transforms.ToTensor()(error_image)
            output_image = Image.open(gen_plt(depthF.data.cpu()[0],sc_c=[0.0,1.2]))
            # output_image.save("/home/ryota/Desktop/test/" +"depthCSS" + str(i) + ".png", quality=95)
            output_image = transforms.ToTensor()(output_image)
                # depthCS_image = Image.open(gen_plt(depth_hidden.data.cpu()[0]))
                # depthCS_image.save("/home/ryota/Desktop/testCS/" +"depthCS" + str(i) + ".png", quality=95)
                # depthC_image = Image.open(gen_plt(depth1.data.cpu()[0]))
                # depthC_image.save("/home/ryota/Desktop/testC/" +"depthC" + str(i) + ".png", quality=95)
            infer_writer.add_image('infer_Image_n', image_now, i + 1)
            infer_writer.add_image('infer_Image_n-1', image_past, i + 1)
            infer_writer.add_image('infer_Depth_GroundTruth', label_image, i + 1)
            infer_writer.add_image('infer_Depth_Output', output_image, i + 1)
            infer_writer.add_image('infer_error_Output', error_image, i + 1)
            infer_writer.add_scalar('running_RMSE', running_RMSE, i + 1)

    # print("Average RMSE:", running_RMSE / (i + 1))
    # print("Average abs_diff:", running_abs_diff / (i + 1))
    # print("Average abs_rel:", running_abs_rel / (i + 1))
    # print("Average inv_rel:", running_inv_rel / (i + 1))
    # print("Average sc_inv:", running_sc_inv / (i + 1))
    # print("Average a1:", running_a1 / (i + 1))
    # print("Average a2:", running_a2 / (i + 1))
    # print("Average a3:", running_a3 / (i + 1))
    # print("Number of data:", i)
    # print("Average time:", running_time / i)
    np.set_printoptions(linewidth=np.inf)
    print(np.hstack((np.mean(results.table_of_results,axis=0),np.std(results.table_of_results,axis=0))))
    results.saveToCSV(results_path)
def inferCorS(inferloader, infer_writer, model, intrinsics_mat,results_path):
    model.eval()
    running_RMSE, running_abs_diff, running_abs_rel, running_inv_rel, running_sc_inv, running_a1, running_a2, running_a3, running_time = 0, 0, 0, 0, 0, 0, 0, 0, 0
    running_time = 0.0
    #print(len(inferloader))
    results=InferResult(len(inferloader))
    with tqdm(inferloader, ncols=150) as pbar:
        results.setPBar(pbar)
        for i, data in enumerate(pbar):
            end = time.time()
            images, motion_mat, depth, vels = data
            depth = depth.to(device)
            images[0] = images[0].to(device)
            images[1] = images[1].to(device)
            motion_mat = motion_mat.to(device)
            vels = vels.to(device)

            depthF = model([images[0], images[1], vels])
            if depthF.dim() == 4:
                depthF = torch.squeeze(depth1, dim=1)



            error_map = torch.abs(depthF-depth)
            loss_RMSE = criterion(depthF, depth)
            output_depth=depthF.cpu()[0].numpy()
            background_depth=depth.cpu()[0].numpy()
            # print(np.ndarray.min(background_depth))
            # max_depth= np.ndarray.max(background_depth)
            # min_depth= np.ndarray.min(background_depth)
            # max_depth_ou= np.ndarray.max(output_depth)
            # min_depth_ou= np.ndarray.min(output_depth)
            #
            # n_background_image=(1/(max_depth-min_depth))*255.0*background_depth
            # n_output_depth=(1/(max_depth_ou-min_depth_ou))*255.0*output_depth
            # n_output_depth[n_output_depth>255]=255.0
            # n_output_depth[n_output_depth<0]=0.0
            # n_background_image[n_background_image>255]=255.0
            # n_background_image[n_background_image<0]=0.0
            # output_image = Image.fromarray(np.uint8(np.round(n_output_depth)))
            # background_image=Image.fromarray(np.uint8(np.round(n_background_image)))
            #
            # (score,diff) = compare_ssim(output_depth, background_depth, full=True)
            # print("SSIM: {}".format(score))
            #
            # fig=plt.figure(figsize=(1,2))
            # fig.add_subplot(1,3,1)
            # plt.imshow(output_image)
            # fig.add_subplot(1,3,2)
            # plt.imshow(background_image)
            # fig.add_subplot(1,3,3)
            # plt.imshow(diff)
            # plt.show()



            loss_metrics = compute_errors(depth, depthF)
            #abs_diff, abs_rel, inv_rel, sc_inv, a1, a2, a3 = loss_metrics
            running_RMSE += loss_RMSE.item()
            results.update_metrics(loss_metrics,loss_RMSE.item(),background_depth,output_depth)

            # running_abs_diff += abs_diff
            # running_abs_rel += abs_rel
            # running_inv_rel += inv_rel
            # running_sc_inv += sc_inv
            # running_a1 += a1
            # running_a2 += a2
            # running_a3 += a3
            # running_time += time.time() - end
            # end = time.time()
            # pbar.set_postfix(OrderedDict(
            #     running_time="{:.4f}".format(running_time / (i + 1)),
            #     RMSE="{:.4f}".format(running_RMSE / (i + 1)),
            #     abs_diff="{:.4f}".format(running_abs_diff / (i + 1)),
            #     sc_inv="{:.4f}".format(running_sc_inv / (i + 1)),
            #     abs_rel="{:.4f}".format(running_abs_rel / (i + 1))
            # ))
            i += 1

            # if i % 1 == 1 - 1:
            image_nows = images[1]
            image_pasts = images[0]
            image_now = vutils.make_grid(
                 image_nows[0], normalize=True, scale_each=True)
            image_past = vutils.make_grid(
                     image_pasts[0], normalize=True, scale_each=True)
            label_image = Image.open(gen_plt(depth.cpu()[0],sc_c=[0.0,1.2]))
                # label_image.save("/home/ryota/Desktop/test/" +"label" + str(i) + ".png", quality=95)
            label_image = transforms.ToTensor()(label_image)
            error_image = Image.open(gen_plt(error_map.cpu()[0],sc_c=[0,0.3]))
            error_image = transforms.ToTensor()(error_image)
            output_image = Image.open(gen_plt(depthF.data.cpu()[0],sc_c=[0.0,1.2]))
            # output_image.save("/home/ryota/Desktop/test/" +"depthCSS" + str(i) + ".png", quality=95)
            output_image = transforms.ToTensor()(output_image)
            #     # depthCS_image = Image.open(gen_plt(depth_hidden.data.cpu()[0]))
            #     # depthCS_image.save("/home/ryota/Desktop/testCS/" +"depthCS" + str(i) + ".png", quality=95)
            #     # depthC_image = Image.open(gen_plt(depth1.data.cpu()[0]))
            #     # depthC_image.save("/home/ryota/Desktop/testC/" +"depthC" + str(i) + ".png", quality=95)
            infer_writer.add_image('infer_Image_n', image_now, i + 1)
            infer_writer.add_image('infer_Image_n-1', image_past, i + 1)
            infer_writer.add_image('infer_Depth_GroundTruth', label_image, i + 1)
            infer_writer.add_image('infer_Depth_Output', output_image, i + 1)
            infer_writer.add_image('infer_error_Output', error_image, i + 1)
            infer_writer.add_scalar('running_RMSE', running_RMSE, i + 1)

    # print("Average RMSE:", running_RMSE / (i + 1))
    # print("Average abs_diff:", running_abs_diff / (i + 1))
    # print("Average abs_rel:", running_abs_rel / (i + 1))
    # print("Average inv_rel:", running_inv_rel / (i + 1))
    # print("Average sc_inv:", running_sc_inv / (i + 1))
    # print("Average a1:", running_a1 / (i + 1))
    # print("Average a2:", running_a2 / (i + 1))
    # print("Average a3:", running_a3 / (i + 1))
    # print("Number of data:", i)
    # print("Average time:", running_time / i)
    np.set_printoptions(linewidth=np.inf)
    print(np.hstack((np.mean(results.table_of_results,axis=0),np.std(results.table_of_results,axis=0))))
    results.saveToCSV(results_path)
## main ##
if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='PyTorch DepthCSx inference on several datasets')
    parser.add_argument("-m", "--mode")
    parser.add_argument("-w", "--weights")
    parser.add_argument("-p", "--path_to_dataset")
    parser.add_argument("-l","--logger_path")
    args = parser.parse_args()

    input_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


    # criterion = EigenLoss()
    criterion = RMSELoss()
    if not(args.logger_path is None):
        save_path=args.logger_path
    else:
        save_path = "/home/ryota/DepthCorrNet/trained_models/"

    if not(args.weights is None):
        PATH_load=args.weights
    else:
        PATH_load ="/home/ryota/DepthCorrNet/trained_models/checkpoint.pth"

    if not(args.path_to_dataset is None):
        dataset_dir=args.path_to_dataset
    else:
        dataset_dir='/home/ryota/baxter_datasets/infer/'

    if args.mode == "css" or args.mode=="CSS":
        # inference
        bs_train = 1
        intrinsics_mat = torch.tensor(
            [[286.02185, 0.0, 240.5], [0.0, 286.02185, 150.5], [0.0, 0.0, 1.0]])
        #intrinsics_mat = torch.tensor(
        #    [[405.7, 0.0, 240.5], [0.0, 405.7, 150.5], [0.0, 0.0, 1.0]])
        intrinsics_mat = intrinsics_mat.repeat(bs_train, 1, 1).cuda().float()
        infer_writer = SummaryWriter(os.path.join(save_path, 'infer'))
    #    PATH_load = "/home/ryota/DepthCorrNet/trained_models/DepthCSS_fixedCS/checkpoint.pth"
        checkpoint = torch.load(PATH_load, map_location='cpu')
        modelC = DepthCorrNet.DepthC().cuda()
        modelC.load_state_dict(checkpoint['model_state_dictC'])
        modelS1 = DepthCorrNet.DepthS().cuda()
        modelS1.load_state_dict(checkpoint['model_state_dictS1'])
        modelS2 = DepthCorrNet.DepthS().cuda()
        modelS2.load_state_dict(checkpoint['model_state_dictS2'])

        infer_list = makelist_infer(dataset_dir)
        infer_data = EgoMotionDataset.EgoMotionTxtDataset(
            dataset_dir, input_transform, infer_list)

        inferloader = torch.utils.data.DataLoader(infer_data, batch_size=1,
                                                  shuffle=False, num_workers=1)
        with torch.no_grad():
            inferCSS(inferloader, infer_writer, modelC,
                  modelS1, modelS2, intrinsics_mat, os.path.join(args.logger_path,"inference_results.csv"),loop=1)

    elif args.mode == "cs" or args.mode=="CS":
        # inference
        bs_train = 1
        intrinsics_mat = torch.tensor(
            [[286.02185, 0.0, 240.5], [0.0, 286.02185, 150.5], [0.0, 0.0, 1.0]])

        intrinsics_mat = intrinsics_mat.repeat(bs_train, 1, 1).cuda().float()
        infer_writer = SummaryWriter(os.path.join(save_path, 'infer'))
        #PATH_load = "/home/ryota/DepthCorrNet/trained_models/DepthCS_fixedC/checkpoint.pth"
        checkpoint = torch.load(PATH_load, map_location='cpu')
        modelC = DepthCorrNet.DepthC().cuda()
        modelC.load_state_dict(checkpoint['model_state_dictC'])
        modelS1 = DepthCorrNet.DepthS().cuda()
        modelS1.load_state_dict(checkpoint['model_state_dictS'])


        infer_list = makelist_infer(dataset_dir)
        infer_data = EgoMotionDataset.EgoMotionTxtDataset(
            dataset_dir, input_transform, infer_list)

        inferloader = torch.utils.data.DataLoader(infer_data, batch_size=1,
                                                  shuffle=False, num_workers=1)
        with torch.no_grad():
            inferCS(inferloader, infer_writer, modelC,
                  modelS1, intrinsics_mat,os.path.join(args.logger_path,"inference_results.csv"))

    elif args.mode == "c":
        # inference
        bs_train = 1
        intrinsics_mat = torch.tensor(
            [[286.02185, 0.0, 240.5], [0.0, 286.02185, 150.5], [0.0, 0.0, 1.0]])
        # intrinsics_mat = torch.tensor(
        #     [[405.7, 0.0, 240.5], [0.0, 405.7, 150.5], [0.0, 0.0, 1.0]])
        intrinsics_mat = intrinsics_mat.repeat(bs_train, 1, 1).cuda().float()
        infer_writer = SummaryWriter(os.path.join(save_path, 'infer'))
        #PATH_load = "/home/ryota/DepthCorrNet/trained_models/DepthC/checkpointC.pth"
        checkpoint = torch.load(PATH_load, map_location='cpu')
        modelC = DepthCorrNet.DepthC().cuda()
        modelC.load_state_dict(checkpoint['model_state_dict'])


        infer_list = makelist_infer(dataset_dir)
        infer_data = EgoMotionDataset.EgoMotionTxtDataset(
            dataset_dir, input_transform, infer_list)

        inferloader = torch.utils.data.DataLoader(infer_data, batch_size=1,
                                                  shuffle=False, num_workers=1)
        with torch.no_grad():
            inferCorS(inferloader, infer_writer, modelC, intrinsics_mat,os.path.join(args.logger_path,"inference_results.csv"))

    elif args.mode == "s":
        # inference
        bs_train = 1
        intrinsics_mat = torch.tensor(
            [[286.02185, 0.0, 240.5], [0.0, 286.02185, 150.5], [0.0, 0.0, 1.0]])
        intrinsics_mat = intrinsics_mat.repeat(bs_train, 1, 1).cuda().float()
        infer_writer = SummaryWriter(os.path.join(save_path, 'infer'))
        #PATH_load = "/home/ryota/DepthCorrNet/trained_models/DepthS/checkpointS.pth"
        checkpoint = torch.load(PATH_load, map_location='cpu')
        modelS = DepthCorrNet.DepthS_solo().cuda()
        modelS.load_state_dict(checkpoint['model_state_dict'])


        infer_list = makelist_infer(dataset_dir)
        infer_data = EgoMotionDataset.EgoMotionTxtDataset(
            dataset_dir, input_transform, infer_list)

        inferloader = torch.utils.data.DataLoader(infer_data, batch_size=1,
                                                  shuffle=False, num_workers=1)
        with torch.no_grad():
            inferCorS(inferloader, infer_writer, modelS, intrinsics_mat,os.path.join(args.logger_path,"inference_results.csv"))

    elif args.mode == "inferD":
        # inference
        infer_writer = SummaryWriter(os.path.join(save_path, 'infer'))
        PATH_infer_model = "/home/ryota/FlowNetPytorch/trained_models/model.pth"
        param = torch.load(PATH_infer_model)
        model3 = FlowNetCEGO.FlowDepthNet(param).cuda()
        model3.load_state_dict(torch.load(PATH_infer_model))
        infer_list = makelist_infer_duran(
            "/home/ryota/baxter_datasets/saccade_r_0_05_0_15_20190202/")
        infer_data = EgoMotionDataset.RealDataset(
            "/home/ryota/baxter_datasets/saccade_r_0_05_0_15_20190202/", input_transform, infer_list)

        inferloader = torch.utils.data.DataLoader(infer_data, batch_size=1,
                                                  shuffle=False, num_workers=1)
        with torch.no_grad():
            infer_duran(inferloader, infer_writer, model3)
    else:
        print("Error Inference mode not well defined")
        sys.exit(1)

# for i, data in enumerate(trainloader, 0):
#
# #     # get the inputs
#     inputs, depth = data
