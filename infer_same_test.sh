#!/bin/bash

args=( "$@" )
#printf '%s\n' "${args[@]}"
filename=${args[-1]}
epochs=${args[-2]}
mode=${args[-3]}
if test -f "$filename"; then
  rm $filename
fi
echo "TYPE "$mode
total=${#args[*]}
root_dir=$(pwd)
for (( i=0; i<=$(( $total -4 )); i++ ))
do
    var=${args[$i]}
    result=$(python3 inferCSX.py -m $mode --weights="/home/"$USER"/trained_models/depth_"$mode"_weights_to_"${var}"/depth_"$mode"_checkpoint_to_"${var}"_epc_"$epochs".pth" --path_to_dataset="/home/"$USER"/baxter_datasets/infer/" --logger_path="/home/"$USER"/trained_models/infer_"${var}"_"$mode"_logger_to_"${var}"_eps_"$epochs"_same_test")
    echo ${var}" "$result >>$filename
done
