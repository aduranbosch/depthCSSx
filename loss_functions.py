from __future__ import division
import torch
from torch import nn
import torch.nn.functional as F
from inverse_warp import inverse_warp


def photometric_reconstruction_loss(tgt_img, ref_imgs,intrinsics,
                                    depth, explainability_mask, pose,
                                    rotation_mode='euler', padding_mode='zeros'):
    def one_scale(depth, explainability_mask):
        assert(explainability_mask is None or depth.size()[2:] == explainability_mask.size()[2:])
        assert(len(pose) == len(ref_imgs))

        reconstruction_loss = 0
        if depth.dim() == 2:
            h, w = depth.size()
            b = 1
        else:
            b, h, w = depth.size()
        downscale = tgt_img.size(2)/h

        # intrinsics = torch.tensor([[286.02185, 0.0, 240.5], [0.0, 286.02185, 150.5],[0.0, 0.0 , 1.0]])
        # intrinsics = intrinsics.repeat(b,1,1).cuda().float()

        tgt_img_scaled = F.interpolate(tgt_img, (h, w), mode='area')
        ref_imgs_scaled = F.interpolate(ref_imgs, (h, w), mode='area')




        ref_img_warped, valid_points = inverse_warp(ref_imgs, depth, pose,
                                                    intrinsics,
                                                    rotation_mode, padding_mode)


        diff = (tgt_img_scaled - ref_img_warped) * valid_points.unsqueeze(1).float()
        # diff = (tgt_img_scaled - ref_img_warped)


        if explainability_mask is not None:
            diff = diff * explainability_mask[:,i:i+1].expand_as(diff)

        reconstruction_loss += diff.abs().mean()
        assert((reconstruction_loss == reconstruction_loss).item() == 1)



        return reconstruction_loss, ref_img_warped, diff

    # warped_results, diff_results = [], []
    # if type(explainability_mask) not in [tuple, list]:
    #     explainability_mask = [explainability_mask]
    # if type(depth) not in [list, tuple]:
    #     depth = [depth]
    #
    # total_loss = 0
    # for d, mask in zip(depth, explainability_mask):
    #     loss, warped, diff = one_scale(d, mask)
    #     total_loss += loss
    #     print("loss:",total_loss)
    #     warped_results.append(warped)
    #     diff_results.append(diff)
    loss, warped, diff = one_scale(depth, explainability_mask=None)
    return loss, warped, diff


def explainability_loss(mask):
    if type(mask) not in [tuple, list]:
        mask = [mask]
    loss = 0
    for mask_scaled in mask:
        ones_var = torch.ones_like(mask_scaled)
        loss += nn.functional.binary_cross_entropy(mask_scaled, ones_var)
    return loss


def smooth_loss(pred_map):
    def gradient(pred):
        D_dy = pred[:, :, 1:] - pred[:, :, :-1]
        D_dx = pred[:, 1:, :] - pred[:, :-1,:]
        return D_dx, D_dy


    loss = 0
    weight = 1.

    dx, dy = gradient(pred_map)
    dx2, dxdy = gradient(dx)
    dydx, dy2 = gradient(dy)
    loss += (dx2.abs().mean() + dxdy.abs().mean() + dydx.abs().mean() + dy2.abs().mean())*weight
    weight /= 2.3  # don't ask me why it works better
    return loss


@torch.no_grad()
def compute_errors(gt, pred, crop=True):
    abs_diff, abs_rel, inv_rel, sc_inv, a1, a2, a3 = 0,0,0,0,0,0,0
    batch_size = gt.size(0)

    '''
    crop used by Garg ECCV16 to reprocude Eigen NIPS14 results
    construct a mask of False values, with the same size as target
    and then set to True values inside the crop
    '''
    if crop:
        crop_mask = gt[0] != gt[0]
        y1,y2 = int(0.40810811 * gt.size(1)), int(0.99189189 * gt.size(1))
        x1,x2 = int(0.03594771 * gt.size(2)), int(0.96405229 * gt.size(2))
        crop_mask[y1:y2,x1:x2] = 1

    for current_gt, current_pred in zip(gt, pred):
        valid = (current_gt > 0) & (current_gt < 80)
        if crop:
            valid = valid & crop_mask

        valid_gt = current_gt[valid]
        valid_pred = current_pred[valid].clamp(1e-3, 80)

        valid_pred = valid_pred * torch.median(valid_gt)/torch.median(valid_pred)

        thresh = torch.max((valid_gt / valid_pred), (valid_pred / valid_gt))
        a1 += (thresh < 1.25).float().mean()
        a2 += (thresh < 1.25 ** 2).float().mean()
        a3 += (thresh < 1.25 ** 3).float().mean()

        abs_diff += torch.mean(torch.abs(valid_gt - valid_pred))
        abs_rel += torch.mean(torch.abs(valid_gt - valid_pred) / valid_gt)

        z_gt = torch.log(valid_gt)
        z_pred = torch.log(valid_pred)
        d = torch.abs(z_gt - z_pred)
        sc_inv += torch.sqrt(torch.mean(d**2) - torch.mean(d)**2)

        inv_rel += torch.mean(torch.abs(1./valid_gt - 1./valid_pred)/1./valid_gt)

    return [metric.item() / batch_size for metric in [abs_diff, abs_rel, inv_rel, sc_inv, a1, a2, a3]]


def compute_depth_errors(gt, pred, crop=True):
    abs_diff, abs_rel, sq_rel, a1, a2, a3 = 0,0,0,0,0,0
    b, h, w = gt.size()
    if pred.size(1) != h:
        pred_upscaled = F.interpolate(pred, (h, w), mode='bilinear', align_corners=False)[:,0]
    else:
        pred_upscaled = pred[0:,]

    '''
    crop used by Garg ECCV16 to reprocude Eigen NIPS14 results
    construct a mask of False values, with the same size as target
    and then set to True values inside the crop
    '''
    if crop:
        crop_mask = gt[0] != gt[0]
        y1,y2 = int(0.40810811 * gt.size(1)), int(0.99189189 * gt.size(1))
        x1,x2 = int(0.03594771 * gt.size(2)), int(0.96405229 * gt.size(2))
        crop_mask[y1:y2,x1:x2] = 1

    for current_gt, current_pred in zip(gt, pred_upscaled):
        valid = (current_gt > 1e-3) & (current_gt < 80)
        if crop:
            valid = valid & crop_mask

        valid_gt = current_gt[valid]
        valid_pred = current_pred[valid].clamp(1e-3, 80)

        # thresh = torch.max((valid_gt / valid_pred), (valid_pred / valid_gt))
        # a1 += (thresh < 1.25).float().mean()
        # a2 += (thresh < 1.25 ** 2).float().mean()
        # a3 += (thresh < 1.25 ** 3).float().mean()

        abs_diff += torch.mean(torch.abs(1.0/valid_gt - 1.0/valid_pred))
        # abs_rel += torch.mean(torch.abs(valid_gt - valid_pred) / valid_gt)

        sq_rel += torch.sqrt(torch.mean(((valid_gt - valid_pred)**2)))

    return abs_diff/b, sq_rel/b


class RMSELoss(nn.Module):
    def __init__(self, eps=1e-7):
        super().__init__()
        self.mse = nn.MSELoss()
        self.eps = eps
    def forward(self,yhat,y):
        # loss = torch.sqrt(self.mse(torch.log(yhat+self.eps),torch.log(y+self.eps)) + self.eps)
        loss = torch.sqrt(self.mse(yhat,y) + self.eps)
        return loss

class EigenLoss(nn.Module):
    def __init__(self, eps=1e-7):
        super().__init__()
        self.mse = nn.MSELoss()
        self.l1 = nn.L1Loss(reduction='elementwise_mean')
        self.eps = eps
    def forward(self,yhat,y):
        # loss = torch.sqrt(self.mse(torch.log(yhat+self.eps),torch.log(y+self.eps)) + self.eps)
        loss = self.mse(yhat,y) - self.l1(yhat,y)**2
        return loss
