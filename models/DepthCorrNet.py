import torch
import torch.nn as nn
from torch.nn.init import kaiming_normal_, constant_
from .util import conv, predict_flow, deconv, crop_like, correlate, correlate_patch, predict_depth, predict_motion_block, convrelu_block
import numpy as np
__all__ = [
    'flownetc', 'flownetc_bn'
]
from pyquaternion import Quaternion

from loss_functions import photometric_reconstruction_loss, explainability_loss, smooth_loss, compute_errors
from ryota_utils import motion_vec2mat


class DepthC(nn.Module):

    def __init__(self,batchNorm=True):
        super(DepthC,self).__init__()

        self.batchNorm = batchNorm
        self.conv1      = conv(False, True,   3,   64, kernel_size=7, stride=2)
        self.conv2      = conv(False, True,  64,  128, kernel_size=5, stride=2)
        self.conv3      = conv(False, True, 128,  256, kernel_size=5, stride=2)
        self.conv_redir = conv(False, True, 256,   32, kernel_size=1, stride=1)
        self.conv_correlation = conv(False, True, 256,  256)

        self.conv3_1 = conv(False, True, 288,  256)
        self.conv4   = conv(False, True, 256,  512, stride=2)
        self.conv4_1 = conv(True, True, 512,  512)
        self.conv5   = conv(True, True, 512,  512, stride=2)
        self.conv5_1 = conv(True, True, 512,  512)
        self.conv6   = conv(True, True, 512, 1024, stride=2)
        self.conv6_1 = conv(True,True, 1024, 1024)

        self.deconv5 = deconv(1024,512)
        self.deconv4 = deconv(1025,256)
        self.deconv3 = deconv(769,128)
        self.deconv2 = deconv(385,128)
        self.deconv1 = deconv(257,32)
        self.deconv0 = deconv(97,1)

        self.predict_depth6 = predict_depth(1024,1)
        self.predict_depth5 = predict_depth(1025,1)
        self.predict_depth4 = predict_depth(769,1)
        self.predict_depth3 = predict_depth(385,1)
        self.predict_depth2 = predict_depth(257,1)
        self.predict_depth1 = predict_depth(97,1)

        self.upsampled_depth6_to_5 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth5_to_4 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth4_to_3 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth3_to_2 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth2_to_1 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth1_to_0 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)



        for m in self.modules():
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.ConvTranspose2d):
                kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                constant_(m.weight, 1)
                constant_(m.bias, 0)

    def forward(self, x):
        img1 = x[0]  #img1 3 channel
        img2 = x[1]  #img2 3 channel
        vel = x[2]   #velocity 3 channel

        out_conv1a = self.conv1(img1)
        # print("conv1:", out_conv1a.shape)

        out_conv2a = self.conv2(out_conv1a)
        out_conv3a = self.conv3(out_conv2a)

        out_conv1b = self.conv1(img2)
        out_conv2b = self.conv2(out_conv1b)
        out_conv3b = self.conv3(out_conv2b)

        out_conv1c = self.conv1(vel)
        out_conv2c = self.conv2(out_conv1c)
        out_conv3c = self.conv3(out_conv2c)
        # print("conv3:", out_conv3a.shape)

        out_conv_redir = self.conv_redir(out_conv3b)

        out_correlation_image = correlate_patch(out_conv3a,out_conv3b,16)
        out_correlation_image_conv1 = self.conv_correlation(out_correlation_image)
        out_correlation_image_conv2 = self.conv_correlation(out_correlation_image_conv1)
        out_correlation_image_conv3 = self.conv_correlation(out_correlation_image_conv2)
        out_correlation_vel = correlate_patch(out_correlation_image_conv3, out_conv3c,16)
        # print(out_correlation_vel.size())
        in_conv3_1 = torch.cat([out_conv_redir, out_correlation_vel], dim=1)

        out_conv3 = self.conv3_1(in_conv3_1)
        # print("conv3:", out_conv3.shape)
        out_conv4 = self.conv4_1(self.conv4(out_conv3))
        # print("conv4:", out_conv4.shape)
        out_conv5 = self.conv5_1(self.conv5(out_conv4))
        # print("conv5:", out_conv5.shape)
        out_conv6 = self.conv6_1(self.conv6(out_conv5))
        # print("convFinal:", out_conv6.shape)

        depth6 = self.predict_depth6(out_conv6)
        depth6_up = crop_like(self.upsampled_depth6_to_5(depth6), out_conv5)
        out_deconv5 = crop_like(self.deconv5(out_conv6), out_conv5)
        # print("out_deconv5",out_deconv5.shape)
        # print("depth6",depth6.shape)

        concat5 = torch.cat((out_conv5,out_deconv5,depth6_up),1)
        depth5 = self.predict_depth5(concat5)
        depth5_up    = crop_like(self.upsampled_depth5_to_4(depth5), out_conv4)
        out_deconv4 = crop_like(self.deconv4(concat5), out_conv4)
        # print("depth5",depth5.shape)

        concat4 = torch.cat((out_conv4,out_deconv4,depth5_up),1)
        depth4 = self.predict_depth4(concat4)
        depth4_up    = crop_like(self.upsampled_depth4_to_3(depth4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(concat4), out_conv3)
        # print("deconv3:", out_deconv3.shape)
        # print("depth4",depth4.shape)

        concat3 = torch.cat((out_conv3,out_deconv3,depth4_up),1)
        depth3 = self.predict_depth3(concat3)
        depth3_up    = crop_like(self.upsampled_depth3_to_2(depth3), out_conv2b)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2b)
        # print("deconv2:", out_deconv2.shape)
        # print("depth3",depth3.shape)

        concat2 = torch.cat((out_conv2b,out_deconv2,depth3_up),1)
        depth2 = self.predict_depth2(concat2)
        depth2_up    = crop_like(self.upsampled_depth2_to_1(depth2), out_conv1b)
        out_deconv1 = crop_like(self.deconv1(concat2), out_conv1b)
        # print("depth2",depth2.shape)

        concat1 = torch.cat((out_conv1b,out_deconv1,depth2_up),1)
        depth1 = self.predict_depth1(concat1)
        # depth1_up = self.upsampled_depth1_to_0(depth1)
        # print("depth1:", depth1.shape)

        return torch.squeeze(depth1,dim=0)

class DepthS_solo(nn.Module):

    def __init__(self,batchNorm=True):
        super(DepthS_solo,self).__init__()

        self.batchNorm = batchNorm
        self.conv1      = conv(False, True,   9,   64, kernel_size=7, stride=2)
        self.conv2      = conv(False, True,  64,  128, kernel_size=5, stride=2)
        self.conv3      = conv(False, True, 128,  256, kernel_size=5, stride=2)


        self.conv3_1 = conv(False, True, 256,  256)
        self.conv4   = conv(False, True, 256,  512, stride=2)
        self.conv4_1 = conv(False, True, 512,  512)
        self.conv5   = conv(False, True, 512,  512, stride=2)
        self.conv5_1 = conv(False, True, 512,  512)
        self.conv6   = conv(False, True, 512, 1024, stride=2)
        self.conv6_1 = conv(False,True, 1024, 1024)

        self.deconv5 = deconv(1024,512)
        self.deconv4 = deconv(1025,256)
        self.deconv3 = deconv(769,128)
        self.deconv2 = deconv(385,128)
        self.deconv1 = deconv(257,32)
        self.deconv0 = deconv(97,1)

        self.predict_depth6 = predict_depth(1024,1)
        self.predict_depth5 = predict_depth(1025,1)
        self.predict_depth4 = predict_depth(769,1)
        self.predict_depth3 = predict_depth(385,1)
        self.predict_depth2 = predict_depth(257,1)
        self.predict_depth1 = predict_depth(97,1)

        self.upsampled_depth6_to_5 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth5_to_4 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth4_to_3 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth3_to_2 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth2_to_1 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth1_to_0 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)



        for m in self.modules():
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.ConvTranspose2d):
                kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                constant_(m.weight, 1)
                constant_(m.bias, 0)

    def forward(self, x):
        img1 = x[0]  #img1 3 channel
        img2 = x[1]  #img2 3 channel
        vel = x[2]   #velocity 3 channel
        inputs = torch.cat((img1,img2,vel),1)


        out_conv1 = self.conv1(inputs)
        # print("conv1:", out_conv1a.shape)

        out_conv2 = self.conv2(out_conv1)
        # print("conv2:", out_conv2.shape)


        out_conv3 = self.conv3_1(self.conv3(out_conv2))
        # print("conv3:", out_conv3.shape)
        out_conv4 = self.conv4_1(self.conv4(out_conv3))
        # print("conv4:", out_conv4.shape)
        out_conv5 = self.conv5_1(self.conv5(out_conv4))
        # print("conv5:", out_conv5.shape)
        out_conv6 = self.conv6_1(self.conv6(out_conv5))
        # print("convFinal:", out_conv6.shape)

        depth6 = self.predict_depth6(out_conv6)
        depth6_up = crop_like(self.upsampled_depth6_to_5(depth6), out_conv5)
        out_deconv5 = crop_like(self.deconv5(out_conv6), out_conv5)
        # print("out_deconv5",out_deconv5.shape)
        # print("depth6",depth6.shape)

        concat5 = torch.cat((out_conv5,out_deconv5,depth6_up),1)
        depth5 = self.predict_depth5(concat5)
        depth5_up    = crop_like(self.upsampled_depth5_to_4(depth5), out_conv4)
        out_deconv4 = crop_like(self.deconv4(concat5), out_conv4)
        # print("depth5",depth5.shape)

        concat4 = torch.cat((out_conv4,out_deconv4,depth5_up),1)
        depth4 = self.predict_depth4(concat4)
        depth4_up    = crop_like(self.upsampled_depth4_to_3(depth4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(concat4), out_conv3)
        # print("deconv3:", out_deconv3.shape)
        # print("depth4",depth4.shape)

        concat3 = torch.cat((out_conv3,out_deconv3,depth4_up),1)
        depth3 = self.predict_depth3(concat3)
        depth3_up    = crop_like(self.upsampled_depth3_to_2(depth3), out_conv2)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2)
        # print("deconv2:", out_deconv2.shape)
        # print("depth3",depth3.shape)

        concat2 = torch.cat((out_conv2,out_deconv2,depth3_up),1)
        depth2 = self.predict_depth2(concat2)
        depth2_up    = crop_like(self.upsampled_depth2_to_1(depth2), out_conv1)
        out_deconv1 = crop_like(self.deconv1(concat2), out_conv1)
        # print("depth2",depth2.shape)

        concat1 = torch.cat((out_conv1,out_deconv1,depth2_up),1)
        depth1 = self.predict_depth1(concat1)
        # depth1_up = self.upsampled_depth1_to_0(depth1)
        # print("depth1:", depth1.shape)

        return torch.squeeze(depth1,dim=0)

class DepthS(nn.Module):

    def __init__(self,batchNorm=True):
        super(DepthS,self).__init__()

        self.batchNorm = batchNorm
        self.conv1   = conv(False, True,   6,   64, kernel_size=7, stride=2)
        self.conv2   = conv(False, True,  64,  128, kernel_size=5, stride=2)
        self.conv3   = conv(False, True, 192,  256, kernel_size=5, stride=2)
        self.conv3_1 = conv(False, True, 256,  256)
        self.conv4   = conv(False, True, 256,  512, stride=2)
        self.conv4_1 = conv(True, True, 512,  512)
        self.conv5   = conv(True, True, 512,  512, stride=2)
        self.conv5_1 = conv(True, True, 512,  512)
        self.conv6   = conv(True, True, 512, 1024, stride=2)
        self.conv6_1 = conv(True, True,1024, 1024)

        self.motion_conv, self.motion_fc = predict_motion_block(1024)
        self.conv1_iter   = conv(False, True, 7,  64, kernel_size=7, stride=2)
        self.conv1_iter_1   = conv(False, True, 64,  64, kernel_size=5, stride=1)

        self.deconv5 = deconv(1024,512)
        self.deconv4 = deconv(1025,256)
        self.deconv3 = deconv(769,128)
        self.deconv2 = deconv(385,128)
        self.deconv1 = deconv(257,32)
        self.deconv0 = deconv(97,1)

        self.predict_depth6 = predict_depth(1024,1)
        self.predict_depth5 = predict_depth(1025,1)
        self.predict_depth4 = predict_depth(769,1)
        self.predict_depth3 = predict_depth(385,1)
        self.predict_depth2 = predict_depth(257,1)
        self.predict_depth1 = predict_depth(97,1)

        self.upsampled_depth6_to_5 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth5_to_4 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth4_to_3 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth3_to_2 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth2_to_1 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth1_to_0 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)

    def forward(self, images, warped, diff, depth_predict):
        out_conv1 = self.conv1(torch.cat((images[0],images[1]),1))
        out_conv2 = self.conv2(out_conv1)

        iter_inputs = torch.cat((warped,diff,depth_predict.unsqueeze(1)),1)
        iter_inputs_conv = self.conv1_iter_1(self.conv1_iter(iter_inputs))
        # print(out_conv2.shape)
        # print(iter_inputs_conv.shape)

        out_conv2_cat = torch.cat((out_conv2,iter_inputs_conv),1)
        out_conv3 = self.conv3_1(self.conv3(out_conv2_cat))


        out_conv4 = self.conv4_1(self.conv4(out_conv3))
        out_conv5 = self.conv5_1(self.conv5(out_conv4))
        out_conv6 = self.conv6_1(self.conv6(out_conv5))
        # print(out_conv6.shape)

        motion_conv = self.motion_conv(out_conv6)
        # print(motion_conv.shape)

        motion = self.motion_fc(
            motion_conv.view(
                motion_conv.size(0),
                128 * 5 * 8
        ))


        depth6 = self.predict_depth6(out_conv6)
        depth6_up = crop_like(self.upsampled_depth6_to_5(depth6), out_conv5)
        out_deconv5 = crop_like(self.deconv5(out_conv6), out_conv5)
        # print("out_deconv5",out_deconv5.shape)
        # print("depth6",depth6.shape)

        concat5 = torch.cat((out_conv5,out_deconv5,depth6_up),1)
        depth5 = self.predict_depth5(concat5)
        depth5_up    = crop_like(self.upsampled_depth5_to_4(depth5), out_conv4)
        out_deconv4 = crop_like(self.deconv4(concat5), out_conv4)
        # print("depth5",depth5.shape)

        concat4 = torch.cat((out_conv4,out_deconv4,depth5_up),1)
        depth4 = self.predict_depth4(concat4)
        depth4_up    = crop_like(self.upsampled_depth4_to_3(depth4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(concat4), out_conv3)
        # print("deconv3:", out_deconv3.shape)
        # print("depth4",depth4.shape)

        concat3 = torch.cat((out_conv3,out_deconv3,depth4_up),1)
        depth3 = self.predict_depth3(concat3)
        depth3_up    = crop_like(self.upsampled_depth3_to_2(depth3), out_conv2)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2)
        # print("deconv2:", out_deconv2.shape)
        # print("depth3",depth3.shape)

        concat2 = torch.cat((out_conv2,out_deconv2,depth3_up),1)
        depth2 = self.predict_depth2(concat2)
        depth2_up    = crop_like(self.upsampled_depth2_to_1(depth2), out_conv1)
        out_deconv1 = crop_like(self.deconv1(concat2), out_conv1)
        # print("depth2S",depth2.shape)

        concat1 = torch.cat((out_conv1,out_deconv1,depth2_up),1)
        depth1 = self.predict_depth1(concat1)
        # depth_scaled = depth1 * motion[:,6].view(-1, 1, 1, 1)
        # depth1_up = self.upsampled_depth1_to_0(depth1)
        # print(depth1.shape)

        return torch.squeeze(depth1,dim=0), motion


class DepthSp(nn.Module):

    def __init__(self,batchNorm=True):
        super(DepthSp,self).__init__()

        self.batchNorm = batchNorm
        self.conv1   = conv(False, True,  10,   64, kernel_size=7, stride=2)
        self.conv2   = conv(False, True,  64,  128, kernel_size=5, stride=2)
        self.conv3   = conv(False, True, 128,  256, kernel_size=5, stride=2)
        self.conv3_1 = conv(True, True, 256,  256)
        self.conv4   = conv(False, True, 256,  512, stride=2)
        self.conv4_1 = conv(True, True, 512,  512)
        self.conv5   = conv(False, True, 512,  512, stride=2)
        self.conv5_1 = conv(True, True, 512,  512)
        self.conv6   = conv(False, True, 512, 1024, stride=2)
        self.conv6_1 = conv(True, True,1024, 1024)

        self.motion_conv, self.motion_fc = predict_motion_block(1024)
        self.conv1_iter   = conv(False, True, 7,  64, kernel_size=7, stride=2)
        self.conv1_iter_1   = conv(False, True, 64,  64, kernel_size=5, stride=1)

        self.deconv5 = deconv(1024,512)
        self.deconv4 = deconv(1025,256)
        self.deconv3 = deconv(769,128)
        self.deconv2 = deconv(385,128)
        self.deconv1 = deconv(257,32)
        self.deconv0 = deconv(97,1)

        self.predict_depth6 = predict_depth(1024,1)
        self.predict_depth5 = predict_depth(1025,1)
        self.predict_depth4 = predict_depth(769,1)
        self.predict_depth3 = predict_depth(385,1)
        self.predict_depth2 = predict_depth(257,1)
        self.predict_depth1 = predict_depth(97,1)

        self.upsampled_depth6_to_5 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth5_to_4 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth4_to_3 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth3_to_2 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth2_to_1 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_depth1_to_0 = nn.ConvTranspose2d(1, 1, 4, 2, 1, bias=False)

    def forward(self, images, diff, depth_predict):

        diff = nn.functional.interpolate(input=diff,size=(300, 480), mode='nearest')
        depth_predict =  nn.functional.interpolate(input=depth_predict.unsqueeze(1),size=(300, 480), mode='nearest')
        out_conv1 = self.conv1(torch.cat((images[0],images[1],diff,depth_predict),1))
        out_conv2 = self.conv2(out_conv1)

        out_conv3 = self.conv3_1(self.conv3(out_conv2))


        out_conv4 = self.conv4_1(self.conv4(out_conv3))
        out_conv5 = self.conv5_1(self.conv5(out_conv4))
        out_conv6 = self.conv6_1(self.conv6(out_conv5))
        # print(out_conv6.shape)

        motion_conv = self.motion_conv(out_conv6)
        # print(motion_conv.shape)

        motion = self.motion_fc(
            motion_conv.view(
                motion_conv.size(0),
                128 * 5 * 8
        ))


        depth6 = self.predict_depth6(out_conv6)
        depth6_up = crop_like(self.upsampled_depth6_to_5(depth6), out_conv5)
        out_deconv5 = crop_like(self.deconv5(out_conv6), out_conv5)
        # print("out_deconv5",out_deconv5.shape)
        # print("depth6",depth6.shape)

        concat5 = torch.cat((out_conv5,out_deconv5,depth6_up),1)
        depth5 = self.predict_depth5(concat5)
        depth5_up    = crop_like(self.upsampled_depth5_to_4(depth5), out_conv4)
        out_deconv4 = crop_like(self.deconv4(concat5), out_conv4)
        # print("depth5",depth5.shape)

        concat4 = torch.cat((out_conv4,out_deconv4,depth5_up),1)
        depth4 = self.predict_depth4(concat4)
        depth4_up    = crop_like(self.upsampled_depth4_to_3(depth4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(concat4), out_conv3)
        # print("deconv3:", out_deconv3.shape)
        # print("depth4",depth4.shape)

        concat3 = torch.cat((out_conv3,out_deconv3,depth4_up),1)
        depth3 = self.predict_depth3(concat3)
        depth3_up    = crop_like(self.upsampled_depth3_to_2(depth3), out_conv2)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2)
        # print("deconv2:", out_deconv2.shape)
        # print("depth3",depth3.shape)

        concat2 = torch.cat((out_conv2,out_deconv2,depth3_up),1)
        depth2 = self.predict_depth2(concat2)
        depth2_up    = crop_like(self.upsampled_depth2_to_1(depth2), out_conv1)
        out_deconv1 = crop_like(self.deconv1(concat2), out_conv1)
        # print("depth2S",depth2.shape)

        concat1 = torch.cat((out_conv1,out_deconv1,depth2_up),1)
        depth1 = self.predict_depth1(concat1)
        # depth_scaled = depth1 * motion[:,6].view(-1, 1, 1, 1)
        # depth1_up = self.upsampled_depth1_to_0(depth1)
        # print(depth1.shape)

        return torch.squeeze(depth1,dim=0), motion


class DepthCSS(nn.Module):

    def __init__(self,batchNorm=True):
        super(DepthCSS,self).__init__()

        self.batchNorm = batchNorm
        self.depthS = DepthS(batchNorm=True)
        self.depthC = DepthC(batchNorm=True)

    def forward(self, images, intrinsics_mat, motion_mat, vel):

        depth1 = self.depthC([images[0],images[1],vel])
        if depth1.dim()==4:
            depth1 = torch.squeeze(depth1,dim=1)

        loss_1, warped_1, diff_1 = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                               depth1, None, motion_mat,
                                                               'euler', 'zeros')
        depth2, motion_vec1 = self.depthS(images,warped_1,diff_1,depth1)
        if depth2.dim()==4:
            depth2 = torch.squeeze(depth2)

        motion_mat2 = motion_vec2mat(motion_vec1)

        loss_2, warped_2, diff_2 = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                               depth2, None, motion_mat2,
                                                               'euler', 'zeros')

        depth3 ,motion_vec2 = self.depthS(images,warped_2,diff_2,depth2)
        if depth3.dim()==4:
            depth3 = torch.squeeze(depth3)

        motion_mat3 = motion_vec2mat(motion_vec2)
        loss_3, _, _ = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                               depth3, None, motion_mat3,
                                                               'euler', 'zeros')

        photo_loss_list = [loss_1, loss_2, loss_3]
        depth_list = [depth1, depth2, depth3]
        return photo_loss_list,depth_list

class DepthCS(nn.Module):

    def __init__(self,batchNorm=True):
        super(DepthCS,self).__init__()

        self.batchNorm = batchNorm
        self.depthSp = DepthSp(batchNorm=True)
        self.depthC = DepthC(batchNorm=True)
        self.depthM = RefinementBlock()

    def forward(self, images, intrinsics_mat, motion_mat, vel):

        depth1 = self.depthC([images[0],images[1],vel])
        if depth1.dim()==4:
            depth1 = torch.squeeze(depth1,dim=1)

        loss_1, warped_1, diff_1 = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                               depth1, None, motion_mat,
                                                               'euler', 'zeros')
        depth2, motion_vec1 = self.depthSp(images,diff_1,depth1)
        # depth2_refine = self.depthM(images[1],depth2)
        if depth2.dim()==4:
            depth2 = torch.squeeze(depth2)


        motion_mat2 = motion_vec2mat(motion_vec1)

        loss_2, warped_2, diff_2 = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                               depth2, None, motion_mat2,
                                                               'euler', 'zeros')




        photo_loss_list = [loss_1, loss_2]
        depth_list = [depth1, depth2]
        return photo_loss_list,depth_list,diff_2

class MotionNet(nn.Module):

    def __init__(self,batchNorm=True):
        super(MotionNet,self).__init__()
        self.motion_conv, self.motion_fc = predict_motion_block(1024)

    def forward(self, middle_outputs_depths):

        motion_conv = self.motion_conv(middle_outputs_depths)
        # print(motion_conv.shape)

        motion = self.motion_fc(
            motion_conv.view(
                motion_conv.size(0),
                128 * 5 * 8
        ))

        return motion




def flownetcego(data=None):
    """FlowNetS model architecture from the
    "Learning Optical Flow with Convolutional Networks" paper (https://arxiv.org/abs/1504.06852)

    Args:
        data : pretrained weights of the network. will create a new one if not set
    """
    model = FlowNetCEGO(batchNorm=False)
    if data is not None:
        model.load_state_dict(data['state_dict'])
    return model


def flownetcego_bn(data=None):
    """FlowNetS model architecture from the
    "Learning Optical Flow with Convolutional Networks" paper (https://arxiv.org/abs/1504.06852)

    Args:
        data : pretrained weights of the network. will create a new one if not set
    """
    model = FlowNetCEGO(batchNorm=True)
    if data is not None:
        model.load_state_dict(data['state_dict'])
    return model

class ArrayToTensor(object):
    """Converts a numpy.ndarray (H x W x C) to a torch.FloatTensor of shape (C x H x W)."""

    def __call__(self, array):
        assert(isinstance(array, np.ndarray))
        array = np.transpose(array, (2, 0, 1))
        # handle numpy array
        tensor = torch.from_numpy(array)
        # put it from HWC to CHW format
        return tensor.float()
