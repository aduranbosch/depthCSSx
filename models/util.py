import torch.nn as nn
import torch.nn.functional as F

try:
    from spatial_correlation_sampler import spatial_correlation_sample
except ImportError as e:
    import warnings
    with warnings.catch_warnings():
        warnings.filterwarnings("default", category=ImportWarning)
        warnings.warn("failed to load custom correlation module"
                      "which is needed for FlowNetC", ImportWarning)


def conv(dropOut, groupNorm, in_planes, out_planes, kernel_size=3, stride=1):
    if dropOut and not groupNorm:
        return nn.Sequential(
            nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=(kernel_size-1)//2, bias=False),
            nn.LeakyReLU(0.1,inplace=True),
            nn.Dropout2d(p=0.5),
        )
    elif groupNorm and not dropOut:
        return nn.Sequential(
            nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=(kernel_size-1)//2, bias=False),
            nn.GroupNorm(32,out_planes),
            nn.LeakyReLU(0.1,inplace=True)
        )
    elif dropOut and groupNorm:
        return nn.Sequential(
            nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=(kernel_size-1)//2, bias=False),
            nn.GroupNorm(32,out_planes),
            nn.LeakyReLU(0.1,inplace=True),
            nn.Dropout2d(p=0.5)
        )
    else:
        return nn.Sequential(
            nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=(kernel_size-1)//2, bias=True),
            nn.LeakyReLU(0.1,inplace=True)
        )


def predict_flow(in_planes):
    return nn.Conv2d(in_planes,2,kernel_size=3,stride=1,padding=1,bias=False)

def predict_depth(in_planes,out_planes):
    return nn.Conv2d(in_planes,out_planes,kernel_size=3,stride=1,padding=1,bias=False)


def deconv(in_planes, out_planes):
    return nn.Sequential(
        nn.ConvTranspose2d(in_planes, out_planes, kernel_size=4, stride=2, padding=1, bias=False),
        nn.LeakyReLU(0.1,inplace=True)
    )


def correlate(input1, input2):
    out_corr = spatial_correlation_sample(input1,
                                          input2,
                                          kernel_size=1,
                                          patch_size=21,
                                          stride=1,
                                          padding=0,
                                          dilation_patch=2)
    # collate dimensions 1 and 2 in order to be treated as a
    # regular 4D tensor
    b, ph, pw, h, w = out_corr.size()
    out_corr = out_corr.view(b, ph * pw, h, w)/input1.size(1)
    return F.leaky_relu_(out_corr, 0.1)

def correlate_patch(input1, input2, patch_size):
    out_corr = spatial_correlation_sample(input1,
                                          input2,
                                          kernel_size=1,
                                          patch_size=16,
                                          stride=1,
                                          padding=0,
                                          dilation_patch=2)
    # collate dimensions 1 and 2 in order to be treated as a
    # regular 4D tensor
    b, ph, pw, h, w = out_corr.size()
    out_corr = out_corr.view(b, ph * pw, h, w)/input1.size(1)
    return F.leaky_relu_(out_corr, 0.1)

def crop_like(input, target):
    if input.size()[2:] == target.size()[2:]:
        return input
    else:
        return input[:, :, :target.size(2), :target.size(3)]

def predict_motion_block( num_inputs , leaky_coef = 0.1):

    """
    :param num_inputs: number of input channels
    :return: rotation, translation and scale
    """

    """
    this block is --> (Conv+ReLU) --> (FC+ReLU) --> (FC+ReLU) --> (FC+ReLU) -->,
    the output is rotation, translation and scale
    """

    conv1 = convrelu_block( num_inputs, 128,  (3, 3), 1, 0.1)

    fc1 = nn.Linear(128*5*8, 512)
    fc2 = nn.Linear(512, 64)
    fc3 = nn.Linear(64, 6)

    leaky_relu1 = nn.LeakyReLU(leaky_coef)
    leaky_relu2 = nn.LeakyReLU(leaky_coef)

    return conv1, \
           nn.Sequential(
               fc1,
               nn.BatchNorm1d(512),
               leaky_relu1,
               fc2,
               nn.BatchNorm1d(64),
               leaky_relu2,
               fc3)

def convrelu_block( num_inputs, num_outputs, kernel_size, stride, leaky_coef ):

    """
    :param num_inputs: number of input channels
    :param num_outputs:  number of output channels
    :param kernel_size:  kernel size
    :param stride:  stride
    :param leaky_coef:  leaky ReLU coefficients
    :return: (Conv + ReLU) block
    """

    """ this block does one 2D convolutions """

    input = num_inputs; output = num_outputs
    k = kernel_size; lc = leaky_coef

    if( not isinstance(stride, tuple)):
        s = (stride, stride)
    else:
        s = stride

    conv1_1 = nn.Conv2d(input, output, k, padding=(k[0] // 2, k[1] // 2), stride=s )
    leaky_relu1_1 = nn.LeakyReLU(lc)

    return nn.Sequential(
        conv1_1,
        nn.BatchNorm2d(output),
        leaky_relu1_1
    )
