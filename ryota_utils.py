import numpy as np
import glob
import os
import random
import sys
import cv2
import matplotlib.pyplot as plt
import torch
import io
import PIL.Image
from pyquaternion import Quaternion
from io import BytesIO
from tensorboardX.utils import figure_to_image
from mpl_toolkits.axes_grid1 import make_axes_locatable

def quat2mat(quat):
    """Convert quaternion coefficients to rotation matrix.

    Args:
        quat: first three coeff of quaternion of rotation. fourht is then computed to have a norm of 1 -- size = [B, 3]
    Returns:
        Rotation matrix corresponding to the quaternion -- size = [B, 3, 3]
    """
    norm_quat = torch.cat([quat[:,:1].detach()*0 + 1, quat], dim=1)
    norm_quat = norm_quat/norm_quat.norm(p=2, dim=1, keepdim=True)
    w, x, y, z = norm_quat[:,0], norm_quat[:,1], norm_quat[:,2], norm_quat[:,3]

    B = quat.size(0)

    w2, x2, y2, z2 = w.pow(2), x.pow(2), y.pow(2), z.pow(2)
    wx, wy, wz = w*x, w*y, w*z
    xy, xz, yz = x*y, x*z, y*z

    rotMat = torch.stack([w2 + x2 - y2 - z2, 2*xy - 2*wz, 2*wy + 2*xz,
                          2*wz + 2*xy, w2 - x2 + y2 - z2, 2*yz - 2*wx,
                          2*xz - 2*wy, 2*wx + 2*yz, w2 - x2 - y2 + z2], dim=1).reshape(B, 3, 3)
    return rotMat

def motion_vec2mat(vec, rotation_mode='quat'):
    """
    Convert 6DoF parameters to transformation matrix.

    Args:s
        vec: 6DoF parameters in the order of tx, ty, tz, rx, ry, rz -- [B, 6]
    Returns:
        A transformation matrix -- [B, 3, 4]
    """
    translation = vec[:, :3].unsqueeze(-1)  # [B, 3, 1]
    rot = vec[:,3:]
    if rotation_mode == 'euler':
        rot_mat = euler2mat(rot)  # [B, 3, 3]
    elif rotation_mode == 'quat':
        rot_mat = quat2mat(rot)  # [B, 3, 3]
    transform_mat = torch.cat([rot_mat, translation], dim=2)  # [B, 3, 4]
    return transform_mat

def split2list(images, split, default_split=0.9):
    # split_values = np.random.uniform(0,1,len(images)) < default_split
    split_values = np.array(range(len(images))) < default_split*len(images)
    train_samples = [sample for sample, split in zip(images, split_values) if split]
    test_samples = [sample for sample, split in zip(images, split_values) if not split]
    return train_samples, test_samples

def makelist(dir,split=0.9):
    global data_list
    data_list = []
    for dirnames in os.listdir(dir):
        addlist_txt(dir,dirnames)

    random.shuffle(data_list)
    return split2list(data_list,split,default_split=split)




def addlist_txt(dir,sub_dir, infer=False):
    global data_list
    # data_list = []
    dir_all = dir + sub_dir + "/"
    file_list = sorted(glob.glob(os.path.join(dir_all, '*.png')))

    if infer==True:
        length = len(file_list)-1
    else:
        length = 1

    for index in range(length):
        img1 = "my_image" + str(index) + ".png"
        img2 = "my_image" + str(index+1) + ".png"


        Mat1 = "log_Tranform_" + str(index+0) + ".txt"
        Mat2 = "log_Tranform_" + str(index+1) + ".txt"

        depth = "log_Depth_" + str(index+1) + ".txt"
        if not (os.path.isfile(os.path.join(dir_all,img1)) and os.path.isfile(os.path.join(dir_all,img2)) \
            and os.path.isfile(os.path.join(dir_all,Mat1)) and os.path.isfile(os.path.join(dir_all,Mat2))\
            and os.path.isfile(os.path.join(dir_all,depth))):
            print("yeaaaa:",os.path.isfile(os.path.join(dir_all,img1)),
                            os.path.isfile(os.path.join(dir_all,img2)),
                            os.path.isfile(os.path.join(dir_all,Mat1)),
                            os.path.isfile(os.path.join(dir_all,Mat2)),
                            os.path.isfile(os.path.join(dir_all,depth)))
            return

        img1 = os.path.join(dir_all, img1)
        img2 = os.path.join(dir_all, img2)
        Mat1 = os.path.join(dir_all, Mat1)
        Mat2 = os.path.join(dir_all, Mat2)
        depth = os.path.join(dir_all, depth)
        data_list.append([[img1,img2],[Mat1,Mat2], depth])

def addlist_duran(dir):
    global data_list
    # data_list = []
    dir_all = dir  + "/"
    file_list = sorted(glob.glob(os.path.join(dir_all, '*.png')))
    for index in range(len(file_list)-1):
        img1 = "camera_image" + str(index) + ".png"
        img2 = "camera_image" + str(index+1) + ".png"
        Mat1 = "camera_image" + str(index+0) + ".png.cam_pose.txt"
        Mat2 = "camera_image" + str(index+1) + ".png.cam_pose.txt"

        img1 = os.path.join(dir_all, img1)
        img2 = os.path.join(dir_all, img2)
        Mat1 = os.path.join(dir_all, Mat1)
        Mat2 = os.path.join(dir_all, Mat2)

        data_list.append([[img1,img2],[Mat1,Mat2]])


def makelist_infer(dir):
    global data_list
    data_list = []
    for dirnames in os.listdir(dir):
        addlist_txt(dir,dirnames,infer=True)
    return data_list

def makelist_infer_duran(dir):
    global data_list
    data_list = []
    addlist_duran(dir)
    return data_list

def gen_plt(map_tensor,sc_c=None):
    map_numpy = map_tensor.numpy()
    # print(map_numpy.shape)
    # map_numpy = np.transpose(map_numpy, (1, 2, 0))
    plt.figure()
    ax = plt.gca()
    im=ax.imshow(map_numpy,cmap="gnuplot2")
    if not (sc_c  is None):
        im.set_clim(sc_c[0],sc_c[1])
    divider=make_axes_locatable(ax)
    cax=divider.append_axes("right",size="5%",pad=0.05)
    plt.colorbar(im,cax=cax)

    buf = io.BytesIO()
    plt.savefig(buf, format='jpg')
    plt.close()
    buf.seek(0)
    return buf
def converToJpgFigure(map_tensor):
    map_numpy = map_tensor.numpy()
    print(map_numpy.shape)

    plt.figure()
    plt.imshow(map_numpy,cmap="gnuplot2")
    plt.colorbar()
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    plt.close()
    buf.seek(0)
    png_image = PIL.Image.open(buf)
    png_image = png_image.convert("RGB")
    buf2=io.BytesIO()
    png_image.save(buf2, format='JPEG')
    buf2.seek(0)
    return buf2

def convertMapToTensor(map_tensor):
    map_numpy = map_tensor.numpy()
    LL=plt.figure()
    plt.imshow(map_numpy,cmap="gnuplot2")
    plt.colorbar()
    return figure_to_image(LL)

def show_depth(depth):
    depth_num = depth.numpy()
    depth_num = np.round(depth_num)
    # depth_num = depth_num[np.newaxis, :, :]
    # put it from CHW to HWC format
    depth_num = np.transpose(depth_num, (1, 2, 0))
    # depth_num = np.round(depth_num)
    depth_num = depth_num.astype(np.uint8)
    cv2.imshow('ground',depth_num)
    cv2.waitKey(1)

def show_depth_outputs(depth):
    depth_num = depth.numpy()
    depth_num = np.round(depth_num)

    # depth_num = depth_num[np.newaxis, :, :]
    # put it from CHW to HWC format
    depth_num = np.transpose(depth_num, (1, 2, 0))
    # depth_num = np.round(depth_num)
    depth_num = depth_num.astype(np.uint8)
    cv2.imshow('output',depth_num)
    cv2.waitKey(1)

def show_image(image):
    image = image.numpy()
    # print(image.shape)
    # depth_num = depth_num[np.newaxis, :, :]
    # put it from CHW to HWC format
    image = np.transpose(image, (1, 2, 0))
    # depth_num = np.round(depth_num)
    image = image.astype(np.uint8)
    cv2.imshow('image',image)
    cv2.waitKey(1)

def hsv_depth(map):
    map_numpy = map.numpy()
    if len(map_numpy.shape)==2:
        map_numpy = map_numpy[np.newaxis, :, :]
    # if len(map_numpy.shape)==4:
    [c,h,w] = map_numpy.shape

    max = np.ndarray.max(map_numpy)
    # max = 255.0
    map_numpy = np.round(179-map_numpy*179.0/max)
    hsvimg = np.zeros([h,w,3])
    hsvimg[:,:,0] = map_numpy
    hsvimg[:,:,1] = np.ones([h,w])*255
    hsvimg[:,:,2] = np.ones([h,w])*255
    hsvimg = np.uint8(hsvimg)
    img_num = cv2.cvtColor(hsvimg, cv2.COLOR_HSV2BGR)
    img_tensor = np.transpose(img_num, (2, 0, 1))
    # handle numpy array
    img_tensor = torch.from_numpy(img_tensor)
    return img_tensor
