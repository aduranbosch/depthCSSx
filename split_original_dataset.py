#!/usr/bin/python
import numpy as np
import os
from distutils.dir_util import copy_tree
import shutil

def split_dataset(source,n_samples_in_source,target,n_samples_target):
    selected_folders=[]
    if not os.path.exists(target):
        os.mkdir(target)
        print("Directory " , target ,  " Created ")
    else:
        print("Directory " , target ,  " already exists")
        print("!!! THE EXISTING DATASET WILL BE EREASED !!!!")
        raw_input("Press Enter to continue...")
        shutil.rmtree(target)
        os.mkdir(target)

    kk=np.linspace(1,n_samples_in_source,n_samples_in_source).astype(np.int16)
    np.random.shuffle(kk)
    randnums=kk[0:n_samples_target]
    no_used=kk[n_samples_target:]
    np.savetxt(target+"/no_used_indexes.csv", no_used, delimiter=";")
    for ind in randnums:
        source_path=source+"/scene_"+str(ind)
        target_path=target+"/scene_"+str(ind)
        print("cp %s -->  %s")%(source_path,target_path)
        copy_tree(source_path,target_path)

if __name__ == "__main__":
    source_path="/home/ryota/baxter_datasets/train_data"
    n_samples_in_target=[500,700,1000,1500,2000]
    n_samples_in_source=10000;
    for i in range(len(n_samples_in_target)):
        target_path="/home/ryota/baxter_datasets/train_data_"+str(n_samples_in_target[i])
        print("NUMBER OF TARGET SAMPLES:................%d")%(n_samples_in_target[i])
        split_dataset(source_path,n_samples_in_source,target_path,n_samples_in_target[i])
