import numpy as np
import sys
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from PIL import Image
from skimage.transform import resize

# dir = "/home/ryota/egoMotionDatasets/near1/my_image0.png"
# img = np.zeros([256,256,3])
# img[:,:,0] = np.ones([256,256])*64/255.0
# img[:,:,1] = np.ones([256,256])*128/255.0
# img[:,:,2] = np.ones([256,256])*192/255.0
# print(img.shape)
# img = cv2.resize(img, dsize=(128, 128), interpolation=cv2.INTER_LINEAR)
#
# cv2.imwrite('color_img.jpg', img)
# cv2.imshow("image", img)
# cv2.waitKey(0)
# cv2.destroyAllWindows()


# dir ="/home/ryota/data/sun3d/brown_bm_1/brown_bm_1/depth/0000001-000000000000.png"
# img = cv2.imread(dir)
# print(img)
# # print(img.dtype)
# print(img.shape)
# print(img[0,0,1])
# cv2.imshow('image',img)
# cv2.waitKey(0)
# img_f = img/255.0
# cv2.imshow('image_f',img_f)
# cv2.waitKey(0)
# print(img_f)
# cv2.destroyAllWindows()


depth ="/home/ryota/baxter_datasets//infer/scene_1/log_Depth_1.txt"
depth = np.loadtxt(depth,delimiter=',', skiprows=0)

depth_gray = depth
max = np.ndarray.max(depth)
min = np.ndarray.min(depth)
# max = 1
depth_gray = depth_gray/max
# depth_gray = resize(depth_gray, (64, 64), anti_aliasing=True)

plt.imshow(depth_gray,cmap="gnuplot2")
plt.colorbar()
plt.show()
depth_norm = np.round(depth*255.0/(max-min))
# depth =  cv2.resize(depth, dsize=(128, 128), interpolation=cv2.INTER_LINEAR)
plt.imshow(Image.fromarray(np.uint8(depth_norm)))
plt.show()

depth_cv = cv2.resize(depth, dsize=(240, 150), interpolation=cv2.INTER_LINEAR)
plt.imshow(Image.fromarray(np.uint8(np.round(depth_cv*255/max))))
plt.show()



h,w = depth.shape
hsvimg = np.zeros([h,w,3])
hsvimg[:,:,0] = depth
hsvimg[:,:,1] = np.ones([h,w])*255
hsvimg[:,:,2] = np.ones([h,w])*255
hsvimg = np.uint8(hsvimg)
img = cv2.cvtColor(hsvimg, cv2.COLOR_HSV2BGR)
# img = cv2.resize(img, dsize=(64, 64), interpolation=cv2.INTER_LINEAR)
# cv2.imshow('image',img)
# cv2.imwrite('/home/ryota/Desktop/depth_img.jpg', img)
# cv2.waitKey(1000)
# cv2.destroyAllWindows()
