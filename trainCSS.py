from __future__ import print_function, division
import os
import torch
import pandas as pd
from skimage import io, transform
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
# from torchvision import transforms,utils
from torchvision import utils
import glob
# Ignore warnings
import warnings
import torch.nn as nn
import argparse
import shutil
import time

import torch
import torch.nn.functional as F
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data as data
import torchvision.transforms as transforms
import models
import datetime
from tensorboardX import SummaryWriter
import numpy as np
from torch.nn.init import kaiming_normal_, constant_
from models.util import conv, predict_flow, deconv, crop_like, correlate, correlate_patch
import sys
if '/opt/ros/kinetic/lib/python2.7/dist-packages' in sys.path:
     sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2
import random
import torchvision.utils as vutils
from models import DepthCorrNet
from datasets import EgoMotionDataset
from ryota_utils import split2list, makelist, makelist_infer, gen_plt, makelist_infer_duran, motion_vec2mat
from ryota_utils import show_depth, show_depth_outputs, hsv_depth,show_image,convertMapToTensor
from inverse_warp import inverse_warp
from loss_functions import photometric_reconstruction_loss, explainability_loss, smooth_loss, compute_errors, RMSELoss, EigenLoss

import shutil as sh
# import skorch
# from skorch import NeuralNetClassifier

import matplotlib as mpl
import matplotlib.image as mpimg
from PIL import Image

from tqdm import tqdm

try:
    from collections import OrderedDict
except ImportError:
    OrderedDict = dict



bs_train = 4 # batch size

## train
def train(trainloader,train_writer,epoch,modelC,modelS1,modelS2,optimizerC,optimizerS1,optimizerS2,intrinsics_mat):
    modelC.eval()
    modelS1.eval()
    modelS2.train()
    running_loss_d1 = 0.0 # RMSE of first depth prediction from DepthC
    running_loss_d2 = 0.0 # RMSE of second depth prediction from first DepthS
    running_loss_d3 = 0.0 # RMSE of final depth prediction from second DepthS
    running_loss_p3 = 0.0 # brighness loss of final outputs
    running_loss_s3 = 0.0 # smoothness loss of final outputs
    running_loss = 0.0 # weighed sum of all losses
    with tqdm(trainloader,ncols=150) as pbar:
        for i, data in enumerate(pbar):

            # data assignment
            images, motion_mat, depth, vels = data
            depth = depth.to(device)
            images[0] =images[0].to(device)
            images[1] =images[1].to(device)
            vels = vels.to(device)
            motion_mat = motion_mat.to(device)

            # DepthC
            depth1 = modelC([images[0],images[1],vels])
            if depth1.dim()==4:
                depth1 = torch.squeeze(depth1,dim=1)

            # To calculate brighness loss (loss_photo), warped image, brighness difference(diff)
            loss_photo1, warped_1, diff_1 = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                                            depth1, None, motion_mat,'euler', 'zeros')

            # 1st DepthS
            depth2, motion_vec2 = modelS1(images, warped_1, diff_1, depth1)
            if depth2.dim()==4:
            	depth2 = torch.squeeze(depth2)

            # To convert translation matrix to vector
            motion_mat2 = motion_vec2mat(motion_vec2)
            loss_photo2, warped_2, diff_2 = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                                            depth2, None, motion_mat2,'euler', 'zeros')

            # 2nd DepthS
            depth3, motion_vec3 = modelS2(images, warped_2, diff_2, depth2)
            if depth3.dim()==4:
                depth3 = torch.squeeze(depth3)

            motion_mat3 = motion_vec2mat(motion_vec3)
            loss_photo3, _, _ = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                               depth3, None, motion_mat3,
                                                               'euler', 'zeros')


            loss_depth3  = criterion(depth3, depth)  # RMSE of depth
            loss_smooth3 = smooth_loss(depth3) # Smootheness loss
            lossS2 = 1*loss_depth3 + 0.01*loss_photo3 + 0.01*loss_smooth3
            loss =  lossS2

            # optimizerC.zero_grad()
            # optimizerS1.zero_grad()
            optimizerS2.zero_grad()

            loss.backward()


            # optimizerC.step()
            # optimizerS1.step()
            optimizerS2.step()
            # print statistics
            running_loss += loss.item()
            # running_loss_d1 += loss_disp1.item()
            # running_loss_d2 += loss_disp2.item()
            running_loss_d3 += loss_depth3.item()
            running_loss_p3 += loss_photo3.item()
            running_loss_s3 += loss_smooth3.item()

            pbar.set_postfix(OrderedDict(
                epoch ="{:>10}".format(epoch+1),
                lossAll ="{:.4f}".format(running_loss/(i+1)),
                loss_s3 ="{:.4f}".format(running_loss_s3/(i+1)),
                loss_p3 ="{:.4f}".format(running_loss_p3/(i+1)),
                loss_d3 ="{:.4f}".format(running_loss_d3/(i+1))
            ))



        train_writer.add_scalar('train_loss_all', running_loss/(i+1), epoch+1)
        train_writer.add_scalar('train_loss_depth3', running_loss_d3/(i+1), epoch+1)
        train_writer.add_scalar('train_loss_smooth3', running_loss_s3/(i+1), epoch+1)
        train_writer.add_scalar('train_loss_photo3', running_loss_p3/(i+1), epoch+1)



        loss_ave = running_loss/(i+1)
        return loss_ave

# test
def test(testloader,test_writer,epoch,modelC,modelS1,modelS2,intrinsics_mat):
    modelC.eval()
    modelS1.eval()
    modelS2.eval()
    running_loss_d1 = 0.0
    running_loss_d2 = 0.0
    running_loss_d3 = 0.0
    running_loss_p1 = 0.0
    running_loss_p2 = 0.0
    running_loss_p3 = 0.0
    running_loss_s1 = 0.0
    running_loss_s2 = 0.0
    running_loss_s3 = 0.0
    running_loss = 0.0
    log_point = len(testloader) -10
    with tqdm(testloader,ncols=150) as pbar:
        for i, data in enumerate(pbar):
            images, motion_mat, depth, vels = data
            depth = depth.to(device)
            images[0] =images[0].to(device)
            images[1] =images[1].to(device)
            motion_mat = motion_mat.to(device)
            vels = vels.to(device)

            depth1 = modelC([images[0],images[1], vels])
            if depth1.dim()==4:
                depth1 = torch.squeeze(depth1,dim=1)

            loss_photo1, warped_1, diff_1 = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                                            depth1, None, motion_mat,'euler', 'zeros')


            depth2, motion_vec2 = modelS1(images, warped_1, diff_1, depth1)
            if depth2.dim()==4:
                depth2 = torch.squeeze(depth2)
            motion_mat2 = motion_vec2mat(motion_vec2)

            loss_photo2, warped_2, diff_2 = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                               depth2, None, motion_mat2,
                                                               'euler', 'zeros')
            depth3, motion_vec3 = modelS2(images, warped_2, diff_2, depth2)
            if depth3.dim()==4:
                depth3 = torch.squeeze(depth3)

            motion_mat3 = motion_vec2mat(motion_vec3)

            loss_photo3, _, _ = photometric_reconstruction_loss(images[1], images[0], intrinsics_mat,
                                                               depth3, None, motion_mat3,
                                                               'euler', 'zeros')

            loss_depth3  = criterion(depth3, depth)
            loss_smooth3 = smooth_loss(depth3)
            lossS2 = 1*loss_depth3 + 0.01*loss_photo3 + 0.01*loss_smooth3
            loss =  lossS2


            running_loss += loss.item()
            running_loss_d3 += loss_depth3.item()
            running_loss_p3 += loss_photo3.item()
            running_loss_s3 += loss_smooth3.item()
            pbar.set_postfix(OrderedDict(
                epoch ="{:>10}".format(epoch+1),
                lossAll ="{:.4f}".format(running_loss/(i+1)),
                loss_s3 ="{:.4f}".format(running_loss_s3/(i+1)),
                loss_p3 ="{:.4f}".format(running_loss_p3/(i+1)),
                loss_d3 ="{:.4f}".format(running_loss_d3/(i+1))
            ))


            if i % log_point == log_point - 1:    # print every 2000 mini-batches
                image_nows = images[1]
                image_pasts = images[0]
                image_now = vutils.make_grid(image_nows[0], normalize=True, scale_each=True)
                image_past = vutils.make_grid(image_pasts[0], normalize=True, scale_each=True)
                #label_image = Image.open(gen_plt(depth.cpu()[0]))
                label_image = convertMapToTensor(depth.cpu()[0])
                #label_image = transforms.ToTensor()(label_image).unsqueeze(0)
                #output_image = Image.open(gen_plt(depth2.data.cpu()[0]))
                #output_image = transforms.ToTensor()(output_image).unsqueeze(0)
                output_image = convertMapToTensor(depth2.data.cpu()[0])
                test_writer.add_image('test_Image_n', image_now, epoch+ 1)
                test_writer.add_image('test_Image_n-1', image_past, epoch+ 1)
                test_writer.add_image('test_Depth_GroundTruth', label_image, epoch+ 1)
                test_writer.add_image('test_Depth_Output', output_image, epoch+ 1)





        loss_ave = running_loss/(i+1)
        test_writer.add_scalar('test_loss_ave', loss_ave, epoch+ 1)
        test_writer.add_scalar('test_loss_depth3', running_loss_d3/(i+1), epoch+1)
        test_writer.add_scalar('test_loss_smooth3', running_loss_s3/(i+1), epoch+1)
        test_writer.add_scalar('test_loss_photo3', running_loss_p3/(i+1), epoch+1)
        return running_loss_d3/(i+1)

## main
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='PyTorch DepthCSS Training on several datasets')
    parser.add_argument("--weight")
    parser.add_argument("--train_data_path")
    parser.add_argument("--initial_checkpoint_path")
    parser.add_argument("--path_to_save_weights")
    parser.add_argument("--path_to_depthCS_checkpoints")
    parser.add_argument("--logger_path")
    parser.add_argument("--epochs")
    args = parser.parse_args()

    if not(args.train_data_path is None):
        root_dir=args.train_data_path
    else:
        root_dir='/home/ryota/baxter_datasets/train_data/'

    print("Train data path "+root_dir)

    if not(args.epochs is None):
        epochs=int(args.epochs)
    else:
        epochs=100
    print("Epochs: "+str(epochs))
    if not(args.initial_checkpoint_path is None):
        PATH_load=args.initial_checkpoint_path
    else:
        PATH_load ="/home/ryota/DepthCorrNet/trained_models/checkpoint.pth"

    print("Initial weights path:"+PATH_load)

    if not(args.path_to_depthCS_checkpoints is None):
        checkpointCS_path=args.path_to_depthCS_checkpoints
    else:
        checkpointCS_path="/home/ryota/DepthCorrNet/trained_models/DepthCS/checkpointC.pth"

    print("path to DepthCS weights "+checkpointCS_path)

    if not(args.logger_path is None):
        save_path=args.logger_path
    else:
        save_path = "/home/ryota/DepthCorrNet/trained_models/"
    print("Logger path:"+args.logger_path)

    if not(args.path_to_save_weights is None):
        PATH=args.path_to_save_weights
        filname=os.path.basename(PATH)
        dirname=os.path.dirname(PATH)
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        sh.copy(PATH_load,PATH)
    else:
        PATH = "/home/ryota/DepthCorrNet/trained_models/checkpoint.pth"
    print("Path to save weights:"+args.path_to_save_weights)
    # To transform RGB to tensor image and normalization
    input_transform = transforms.Compose([
        transforms.ToTensor(),
         transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])

    # train datasets path
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")



    if args.weight=="learned": # To succeed learned weight for training
        PATH_load ="/home/ryota/DepthCorrNet/trained_models/checkpoint.pth"
        checkpoint = torch.load(PATH_load)

        modelC = DepthCorrNet.DepthC().cuda()
        modelC.load_state_dict(checkpoint['model_state_dictC'])
        modelS1 = DepthCorrNet.DepthS().cuda()
        modelS1.load_state_dict(checkpoint['model_state_dictS1'])
        modelS2 = DepthCorrNet.DepthS().cuda()
        modelS2.load_state_dict(checkpoint['model_state_dictS2'])

        optimizerC = torch.optim.Adam(modelC.parameters(), 0.0001,
                                 betas=(0.9, 0.999),weight_decay=0.0001)
        optimizerS1 = torch.optim.Adam(modelS1.parameters(), 0.0001,
                                 betas=(0.9, 0.999),weight_decay=0.0001)
        optimizerS2 = torch.optim.Adam(modelS2.parameters(), 0.0001,
                                 betas=(0.9, 0.999),weight_decay=0.0001)
        optimizerC.load_state_dict(checkpoint['optimizer_state_dictC'])
        optimizerS1.load_state_dict(checkpoint['optimizer_state_dictS1'])
        optimizerS2.load_state_dict(checkpoint['optimizer_state_dictS2'])
        epoch_s = 13
        loss_ave_test_best = checkpoint['loss']
    else: # To train withought prelearned weights
        #PATH_load ="/home/ryota/DepthCorrNet/trained_models/DepthCS_fixedC/checkpoint.pth"
        modelC = DepthCorrNet.DepthC().cuda()
        checkpoint = torch.load(checkpointCS_path, map_location="cpu")
        # checkpointC = torch.load("/home/ryota/FlowNetPytorch/trained_models/DepthC8S_30e/checkpointC.pth")
        modelC.load_state_dict(checkpoint['model_state_dictC']) # weights of DepthC are fixed
        modelS1 = DepthCorrNet.DepthS().cuda()
        modelS1.load_state_dict(checkpoint['model_state_dictS']) # weights of 1st DepthS are fixed
        modelS2 = DepthCorrNet.DepthS().cuda()
        modelS2.load_state_dict(checkpoint['model_state_dictS'])  # weights of 2nd DepthS are learned

        optimizerC = torch.optim.Adam(modelC.parameters(), 0.0001,
                                 betas=(0.9, 0.999),weight_decay=0.00001)
        optimizerS1 = torch.optim.Adam(modelS1.parameters(), 0.0001,
                                 betas=(0.9, 0.999),weight_decay=0.00001)
        optimizerS2 = torch.optim.Adam(modelS2.parameters(), 0.0001,
                                 betas=(0.9, 0.999),weight_decay=0.00001)
        epoch_s = 0
        loss_ave_test_best = 10000000000000

    # root mean squre error of depth as an objective function
    criterion = RMSELoss()

    # Path for checkpoint
    #PATH = "/home/ryota/DepthCorrNet/trained_models/checkpoint.pth"



    schedulerS2 = torch.optim.lr_scheduler.ExponentialLR(optimizerS2, 0.95)
    #save_path = "/home/ryota/DepthCorrNet/trained_models/"

    # To split dataset into 80 % for training and 20% for validation
    train_list, test_list = makelist(root_dir,split=0.8)

    train_data = EgoMotionDataset.EgoMotionTxtDataset(root_dir,input_transform, train_list)
    test_data = EgoMotionDataset.EgoMotionTxtDataset(root_dir,input_transform,test_list)
    trainloader = torch.utils.data.DataLoader(train_data, batch_size=bs_train, shuffle=True, num_workers=1)
    testloader = torch.utils.data.DataLoader(test_data, batch_size=bs_train,shuffle=False, num_workers=1)

    # camera intrinsic parameters
    intrinsics_mat = torch.tensor([[286.02185, 0.0, 240.5], [0.0, 286.02185, 150.5],[0.0, 0.0 , 1.0]])
    intrinsics_mat = intrinsics_mat.repeat(bs_train,1,1).cuda().float()

    # writers for tensorboard
    train_writer = SummaryWriter(os.path.join(save_path,'train'))
    test_writer = SummaryWriter(os.path.join(save_path,'test'))

    for epoch in range(0,epochs):

        schedulerS2.step()
        # train
        loss_ave_train =  train(trainloader,train_writer,epoch,modelC,modelS1,modelS2,optimizerC,optimizerS1,optimizerS2,intrinsics_mat)
        # validation and save weights
        with torch.no_grad():
            loss_ave_test = test(testloader,test_writer,epoch,modelC,modelS1,modelS2,intrinsics_mat)
            #if loss_ave_test_best > loss_ave_test:
            if (epoch+1)%6==0:
                loss_ave_test_best = loss_ave_test
                print("save weights and optimizer,loss_best=",loss_ave_test_best)
                torch.save({
                            'epoch': epoch,
                            'model_state_dictC': modelC.state_dict(),
                            'model_state_dictS1': modelS1.state_dict(),
                            'model_state_dictS2': modelS2.state_dict(),
                            # 'optimizer_state_dictC': optimizerC.state_dict(),
                            # 'optimizer_state_dictS1': optimizerS1.state_dict(),
                            'optimizer_state_dictS2': optimizerS2.state_dict(),
                            'loss': loss_ave_test_best,
                            }, PATH)
