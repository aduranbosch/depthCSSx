
import os
import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import utils
import glob
import argparse
import time

import torch
import torch.nn.functional as F
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torchvision.transforms as transforms
import torch.optim as optim
from tensorboardX import SummaryWriter
import numpy as np
import warnings

import sys
if '/opt/ros/kinetic/lib/python2.7/dist-packages' in sys.path:
    sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2
import torchvision.utils as vutils
from models import DepthCorrNet
from datasets import EgoMotionDataset
from ryota_utils import split2list, makelist,  makelist_infer, gen_plt, makelist_infer_duran, motion_vec2mat
from ryota_utils import show_depth, show_depth_outputs, hsv_depth,show_image,converToJpgFigure,convertMapToTensor
from inverse_warp import inverse_warp
from loss_functions import photometric_reconstruction_loss, explainability_loss, smooth_loss, compute_errors, RMSELoss, EigenLoss


from tqdm import tqdm
from PIL import Image
try:
    from collections import OrderedDict
except ImportError:
    OrderedDict = dict

import shutil as sh

# train / validation function
def train(trainloader,train_writer,epoch,model,optimizer):
    model.train() # Additional DepthS is trained
    running_loss = 0.0 # sum of all loss functions

    with tqdm(trainloader,ncols=100) as pbar:
        for i, data in enumerate(pbar):
            # data assinments
            images, motion_mat, depth, vels = data
            depth = depth.to(device)
            images[0] =images[0].to(device)
            images[1] =images[1].to(device)
            vels = vels.to(device)
            motion_mat = motion_mat.to(device)

            depth1 = model([images[0],images[1], vels])
            if depth1.dim()==4:
                depth1 = torch.squeeze(depth1,dim=1)

            loss_depth  = criterion(depth1, depth)

            loss =  loss_depth

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            running_loss += loss.item()


            pbar.set_postfix(OrderedDict(
                epoch ="{:>10}".format(epoch+1),
                loss_all = "{:.4f}".format(running_loss/(i+1))
                ))



    loss_ave = running_loss/(i+1)
    train_writer.add_scalar('train_loss_ave', loss_ave, epoch+ 1)


    return loss_ave

# test
def test(testloader,test_writer,epoch,model):
    model.eval()
    running_loss = 0.0

    log_point = len(testloader) -10
    with tqdm(testloader,ncols=100) as pbar:
        for i, data in enumerate(pbar):
            # get the inputs
            images, motion_mat, depth, vels = data

            depth = depth.to(device)
            images[0] =images[0].to(device)
            images[1] =images[1].to(device)
            motion_mat = motion_mat.to(device)
            vels = vels.to(device)




            depth1 = model([images[0],images[1], vels])
            if depth1.dim()==4:
                depth1 = torch.squeeze(depth1,dim=1)


            loss_depth  = criterion(depth1, depth)
            loss =  loss_depth

            running_loss += loss.item()


            pbar.set_postfix(OrderedDict(
                epoch ="{:>10}".format(epoch+1),
                loss_all = "{:.4f}".format(running_loss/(i+1))
                ))


            if i % log_point == log_point - 1:    # print every 2000 mini-batches
                image_nows = images[1]
                image_pasts = images[0]
                image_now = vutils.make_grid(image_nows[0], normalize=True, scale_each=True)
                image_past = vutils.make_grid(image_pasts[0], normalize=True, scale_each=True)
                label_image = convertMapToTensor(depth.cpu()[0])
                #label_image = transforms.ToTensor()(label_image).unsqueeze(0)
                output_image = convertMapToTensor(depth1.data.cpu()[0])
                #output_image = transforms.ToTensor()(output_image).unsqueeze(0)
                test_writer.add_image('test_Image_n', image_now, epoch+ 1)
                test_writer.add_image('test_Image_n-1', image_past, epoch+ 1)
                test_writer.add_image('test_Image', image_now, epoch+ 1)
                test_writer.add_image('test_Depth_GroundTruth', label_image, epoch+ 1)
                test_writer.add_image('test_Depth_Output', output_image, epoch+ 1)







    loss_ave = running_loss/(i+1)
    test_writer.add_scalar('test_loss_ave', loss_ave, epoch+ 1)
    return loss_ave




## main
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='PyTorch DepthCS Training on several datasets')
    parser.add_argument("-m", "--mode")
    parser.add_argument("--weight")
    parser.add_argument("--train_data_path")
    parser.add_argument("--initial_checkpoint_path")
    parser.add_argument("--path_to_save_weights")
    parser.add_argument("--logger_path")
    parser.add_argument("--epochs")
    args = parser.parse_args()

    bs_train = 4 # batch size


    #Parameters parsers
    print(args)

    if not(args.train_data_path is None):
        root_dir=args.train_data_path
    else:
        root_dir='/home/ryota/baxter_datasets/train_data/'
    print("Train data path "+root_dir)

    if not(args.epochs is None):
        epochs=int(args.epochs)
    else:
        epochs=100
    print("Epochs: "+str(epochs))

    if not(args.initial_checkpoint_path is None):
        PATH_load=args.initial_checkpoint_path
    else:
        PATH_load ="/home/ryota/DepthCorrNet/trained_models/checkpoint.pth"

    print("Initial weights path:"+PATH_load)

    print("Mode:"+args.mode)


    if args.mode=="c":
        if args.weight=="learned":
            print("Learned weights are used")
            #PATH_load ="/home/ryota/DepthCorrNet/trained_models/checkpoint.pth"
            checkpoint = torch.load(PATH_load)

            model = DepthCorrNet.DepthC().cuda()
            model.load_state_dict(checkpoint['model_state_dict'])
            # optimizerC.load_state_dict(checkpoint['optimizer_state_dictC'])
            optimizer = torch.optim.Adam(model.parameters(), 0.0001,
                                     betas=(0.9, 0.999),weight_decay=0.00001)
            epoch_s = 0
            # loss_ave_test_best = checkpoint['loss']
            loss_ave_test_best = 10000000000000

        else:
            model = DepthCorrNet.DepthC().cuda()
            optimizer = torch.optim.Adam(model.parameters(), 0.0001,
                                     betas=(0.9, 0.999),weight_decay=0.00001)
            epoch_s = 0
            loss_ave_test_best = 10000000000000

    if args.mode=="s":
        if args.weight=="learned":
            print("Learned weights are used")
            #PATH_load ="/home/ryota/DepthCorrNet/trained_models/checkpoint.pth"
            checkpoint = torch.load(PATH_load)

            model = DepthCorrNet.DepthS_solo().cuda()
            model.load_state_dict(checkpoint['model_state_dict'])
            # optimizerC.load_state_dict(checkpoint['optimizer_state_dictC'])
            optimizer = torch.optim.Adam(model.parameters(), 0.0001,
                                     betas=(0.9, 0.999),weight_decay=0.00001)
            epoch_s = 0
            # loss_ave_test_best = checkpoint['loss']
            loss_ave_test_best = 10000000000000

        else:
            print("Zero weights are used")
            model = DepthCorrNet.DepthS_solo().cuda()
            optimizer = torch.optim.Adam(model.parameters(), 0.0001,
                                     betas=(0.9, 0.999),weight_decay=0.00001)
            epoch_s = 0
            loss_ave_test_best = 10000000000000

    if not(args.path_to_save_weights is None):
        PATH=args.path_to_save_weights
        filname=os.path.basename(PATH)
        dirname=os.path.dirname(PATH)
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        sh.copy(PATH_load,PATH)
    else:
        PATH = "/home/ryota/DepthCorrNet/trained_models/checkpoint.pth"
    print("Path to save weights:"+args.path_to_save_weights)

    if not(args.logger_path is None):
        save_path=args.logger_path
    else:
        save_path = "/home/ryota/DepthCorrNet/trained_models/"

    print("Logger path:"+args.logger_path)

    #input("Press Enter to start training...")
    # criterion = EigenLoss()
    criterion = RMSELoss()
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    input_transform = transforms.Compose([
        transforms.ToTensor(),
         transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])



    train_list, test_list = makelist(root_dir,split=0.8)

    train_data = EgoMotionDataset.EgoMotionTxtDataset(root_dir,input_transform, train_list)
    test_data = EgoMotionDataset.EgoMotionTxtDataset(root_dir,input_transform,test_list)
    trainloader = torch.utils.data.DataLoader(train_data, batch_size=bs_train,
    shuffle=True, num_workers=1)
    testloader = torch.utils.data.DataLoader(test_data, batch_size=bs_train,
    shuffle=False, num_workers=1)

    intrinsics_mat = torch.tensor([[286.02185, 0.0, 240.5], [0.0, 286.02185, 150.5],[0.0, 0.0 , 1.0]])
    intrinsics_mat = intrinsics_mat.repeat(bs_train,1,1).cuda().float()
    train_writer = SummaryWriter(os.path.join(save_path,'train'))
    test_writer = SummaryWriter(os.path.join(save_path,'test'))
    for epoch in range(0,epochs):

        loss_ave_train =  train(trainloader,train_writer,epoch,model,optimizer)
        with torch.no_grad():
            loss_ave_test = test(testloader,test_writer,epoch,model)
            #if loss_ave_test_best > loss_ave_test:
            if epoch%5==0:
                loss_ave_test_best = loss_ave_test
                print("save weights and optimizer,best:",loss_ave_test_best)
                torch.save({
                            'epoch': epoch,
                            'model_state_dict': model.state_dict(),
                            'optimizer_state_dict': optimizer.state_dict(),
                            'loss': loss_ave_test_best,
                            }, PATH)
