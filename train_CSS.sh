#!/bin/bash

args=( "$@" )
#printf '%s\n' "${args[@]}"


#./trainCorS.py -m ${args[-1]} --train_data_path /home/$USER/baxter_datasets/train_data_$1/ --path_to_save_weights "/home/$USER/trained_models/depth_$2_weights_to_$1/depth_$2_checkpoint_to_$1.pth" --logger_path "/home/$USER/trained_models/depth_$2_logger_to_$1"
total=${#args[*]}
epochs=${args[-1]}
trained_epochs=${args[-2]}
root_dir=$(pwd)
for (( i=0; i<=$(( $total -3 )); i++ ))
do
    var=${args[$i]}
    python3 trainCSS.py --epochs=$epochs --train_data_path="/home/"$USER"/baxter_datasets/train_data_"${var}"/" --path_to_save_weights="/home/"$USER"/trained_models/depth_css_weights_to_"${var}"/depth_css_checkpoint_to_"${var}"_epcs_"$epochs".pth" --logger_path="/home/"$USER"/trained_models/depth_css_logger_to_"${var}"_epcs_"$epochs --initial_checkpoint_path=$root_dir"/trained_models/checkpoint.pth" --path_to_depthCS_checkpoints="/home/"$USER"/trained_models/depth_CS_weights_to_"${var}"/depth_CS_checkpoint_to_"${var}"_epcs_"$trained_epochs".pth"
done
