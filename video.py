import glob
import os
import sys
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2

fourcc = cv2.VideoWriter_fourcc('m','p','4','v')
video = cv2.VideoWriter('/home/ryota/Desktop/depthCS_no_smooth.mp4', fourcc, 5.0, (640, 480))
file_list = sorted(glob.glob('/home/ryota/Desktop/test/output*'))
print(len(file_list))
for i in range(0, len(file_list)):
    img = cv2.imread(os.path.join('/home/ryota/Desktop/test/','output'+ str(i)+'.png'))
    img = cv2.resize(img, dsize=(640, 480), interpolation=cv2.INTER_LINEAR)
    video.write(img)

video.release()


for dirnames in os.listdir('/media/ryota/TOSHIBA EXT/infer_nofloats/'):
    dir_all = '/media/ryota/TOSHIBA EXT/infer_nofloats/' + dirnames + "/"
    file_list = sorted(glob.glob(os.path.join(dir_all, '*.png')))

    for index in range(len(file_list)-1):
        img = cv2.imread(os.path.join(dir_all, 'my_image'+str(index+1)+'.png'))
        img = cv2.resize(img, dsize=(640, 480), interpolation=cv2.INTER_LINEAR)
        video.write(img)
